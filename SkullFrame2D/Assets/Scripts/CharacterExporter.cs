using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using SimpleFileBrowser;

public class CharacterExporter : MonoBehaviour{

    private static CharacterExporter instance;

    private Camera cam;
    private bool takeScreenshotNextFrame;

    void Start(){
		FileBrowser.SetDefaultFilter( ".png" );
	}

    private void Awake(){
        instance = this;
        cam = gameObject.GetComponent<Camera>();
    }

    private void OnPostRender(){
        if(takeScreenshotNextFrame){
            takeScreenshotNextFrame = false;
            RenderTexture renderTex = cam.targetTexture;

            Texture2D renderResult = new Texture2D(renderTex.width, renderTex.height, TextureFormat.ARGB32, false);
            Rect rect = new Rect(0, 0, renderTex.width, renderTex.height);
            renderResult.ReadPixels(rect, 0, 0);

            // byte[] bytes = renderResult.EncodeToPNG();
            // FileBrowserHelpers.WriteBytesToFile(targetPath, bytes);
            // System.IO.File.WriteAllBytes(path + "/ExportedCharacter.png", bytes);
            // Debug.Log("Saved screenshot to: " + Application.dataPath + "/Resources/Character.png");

            byte[] bytes = renderResult.EncodeToPNG();
            FileBrowser.ShowSaveDialog( ( paths ) =>{
                string targetPath = paths[0];
                FileBrowserHelpers.WriteBytesToFile(targetPath, bytes);
            }, null, FileBrowser.PickMode.Files, false, "C:\\", "ExportedCharacter.png", "Save As", "Save" );

            RenderTexture.ReleaseTemporary(renderTex);
            cam.targetTexture = null;
        }
    }

    private void TakeScreenshot(){
        cam.targetTexture = RenderTexture.GetTemporary(Screen.width, Screen.height, 24);
        takeScreenshotNextFrame = true;
    }

    public static void TakeScreenshot_Static(){
        instance.TakeScreenshot();
    }
}
