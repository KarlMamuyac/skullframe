using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PoseSelector : MonoBehaviour
{
    [SerializeField]
    private Dropdown dropdown;

    [SerializeField]
    private Animator charAnim;

    [SerializeField]
    private GameObject poseSelectorPanel;

    // ArrayList of all poses
    private List<string> poses = new List<string>();

    void Start(){
        CreatePoseOptions(charAnim);
        poseSelectorPanel.SetActive(false);
    }

    public void CreatePoseOptions (Animator charAnimator){

        List<Dropdown.OptionData> clipLabels = new List<Dropdown.OptionData>();
        foreach(AnimationClip clip in charAnimator.runtimeAnimatorController.animationClips){
            Debug.Log(clip.name);
            Dropdown.OptionData data = new Dropdown.OptionData(clip.name);
            clipLabels.Add(data);

            // Add to list of poses
            poses.Add(clip.name);
        }

        dropdown.options = clipLabels;

        dropdown.onValueChanged.AddListener(optionIndex =>{
            string label = poses[optionIndex];
            charAnim.Play("Base." + label, 0, 0f);
        }); 
    }
}
