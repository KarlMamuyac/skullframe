using System.Linq;
using Unity.Collections;
using UnityEngine;
using UnityEngine.U2D.Animation;
using UnityEngine.U2D;
using UnityEngine.Rendering;
// using UnityEditor;

public class Swapper : MonoBehaviour{
    #region Inspector

    [SerializeField]
    private SpriteLibrary spriteLibrary = default;

    [SerializeField]
    private SpriteResolver targetResolver = default;

    [SerializeField]
    private string targetCategory = default;

    [SerializeField]
    private GameObject pngSprite;

    private SpriteRenderer pngRenderer;

    #endregion


    #region Properties

    private SpriteLibraryAsset LibraryAsset => spriteLibrary.spriteLibraryAsset;

    #endregion


    #region Methods

    void Start() {
        pngRenderer = pngSprite.GetComponent<SpriteRenderer>();
    }

    public void SelectRandom (){
        string[] labels =
        LibraryAsset.GetCategoryLabelNames(targetCategory).ToArray();
        int index = Random.Range(0, labels.Length);
        string label = labels[index];

        targetResolver.SetCategoryAndLabel(targetCategory, label);
    }

    public void LoadUploadedSprite (string customSpritePath, byte[] bytes){

        // Sprite customSprite = Resources.Load<Sprite>(customSpritePath);
        // texture
        Texture2D texture = new Texture2D(2, 2);
        texture.LoadImage(bytes);
        Sprite customSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        

        if(!pngSprite.activeSelf){
            pngSprite.SetActive(true);
            pngRenderer.sprite = customSprite;
        }else{
            pngRenderer.sprite = customSprite;
        }

        // Duplicate bones and poses
        // string referenceLabel = targetResolver.GetLabel();
        // Sprite referenceSprite = spriteLibrary.GetSprite(targetCategory, referenceLabel);
        // SpriteBone[] bones = referenceSprite.GetBones();
        // NativeArray<Matrix4x4> poses = referenceSprite.GetBindPoses();

        // customSprite.SetBones(bones);
        // customSprite.SetBindPoses(poses);

        // // Inject new sprite
        // string newCustomLabel = "new" + customLabel;
        // spriteLibrary.AddOverride(customSprite, targetCategory, newCustomLabel);
        targetResolver.SetCategoryAndLabel(targetCategory, "Empty");


        
    }

    // public void LoadUploadedSpriteEditor(string customSpritePath, string customLabel){

    //     Sprite customSprite = Resources.Load<Sprite>(customSpritePath);

    //     // Duplicate bones and poses
    //     string referenceLabel = targetResolver.GetLabel();
    //     Sprite referenceSprite = spriteLibrary.GetSprite(targetCategory, referenceLabel);
    //     SpriteBone[] bones = referenceSprite.GetBones();
    //     NativeArray<Matrix4x4> poses = referenceSprite.GetBindPoses();

    //     customSprite.SetBones(bones);
    //     customSprite.SetBindPoses(poses);

    //     // Inject new sprite
    //     string newCustomLabel = "new" + customLabel;
    //     spriteLibrary.AddOverride(customSprite, targetCategory, newCustomLabel);
    //     targetResolver.SetCategoryAndLabel(targetCategory, newCustomLabel);
        
    // }

    #endregion
}
