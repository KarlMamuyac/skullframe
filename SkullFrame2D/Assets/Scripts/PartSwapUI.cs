using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PartSwapUI : MonoBehaviour
{
    [SerializeField]
    private GameObject swapperPanel;

    [SerializeField]
    private GameObject faceSwapperPanel;

    [SerializeField]
    private GameObject bodySwapperPanel;

    [SerializeField]
    private Button bodyPanelBtn;

    [SerializeField]
    private Button facePanelBtn;

    private Image bodyBtnColor;
    private Image faceBtnColor;
    private TMP_Text bodyBtnText;
    private TMP_Text faceBtnText;

    private Color activeBtnColor;
    private Color inactiveBtnColor;
    private Color activeTxtColor;
    private Color inactiveTxtColor;

    public string activeBtnColorHex;
    public string inactiveBtnColorHex;
    public string activeTxtColorHex;
    public string inactiveTxtColorHex;

    void Start(){
        SetUIComponent();
        SetColorParameters();
        SetDefaultPanels();
    }

    void SetDefaultPanels(){
        SetBodyPanelActive();
    }

    void SetUIComponent(){
        bodyBtnColor = bodyPanelBtn.GetComponent<Image>();
        faceBtnColor = facePanelBtn.GetComponent<Image>();
        bodyBtnText = bodyPanelBtn.GetComponentInChildren<TMP_Text>();
        faceBtnText = facePanelBtn.GetComponentInChildren<TMP_Text>();
    }

    void SetColorParameters(){
        activeBtnColor = new Color(HexToColor(activeBtnColorHex).r, HexToColor(activeBtnColorHex).g, HexToColor(activeBtnColorHex).b, 1f);
        inactiveBtnColor = new Color(HexToColor(inactiveBtnColorHex).r, HexToColor(inactiveBtnColorHex).g, HexToColor(inactiveBtnColorHex).b, 1f);
        activeTxtColor = new Color(HexToColor(activeTxtColorHex).r, HexToColor(activeTxtColorHex).g, HexToColor(activeTxtColorHex).b, 1f);
        inactiveTxtColor = new Color(HexToColor(inactiveTxtColorHex).r, HexToColor(inactiveTxtColorHex).g, HexToColor(inactiveTxtColorHex).b, 1f);
    }

    Color HexToColor(string hex){
        Color color;
        ColorUtility.TryParseHtmlString(hex, out color);
        return color;
    }

    public void SetBodyPanelActive(){
        bodyBtnColor.color = activeBtnColor;
        bodyBtnText.color = activeTxtColor;
        faceBtnColor.color = inactiveBtnColor;
        faceBtnText.color = inactiveTxtColor;
        faceSwapperPanel.SetActive(false);
        bodySwapperPanel.SetActive(true);
    }

    public void SetFacePanelActive(){
        bodyBtnColor.color = inactiveBtnColor;
        bodyBtnText.color = inactiveTxtColor;
        faceBtnColor.color = activeBtnColor;
        faceBtnText.color = activeTxtColor;
        faceSwapperPanel.SetActive(true);
        bodySwapperPanel.SetActive(false);
    }

    public void ChangeSwapperPanel(string panelName){
        switch (panelName)
        {
            case "Face":
                faceSwapperPanel.SetActive(true);
                bodySwapperPanel.SetActive(false);
                SetFacePanelActive();
                break;
            case "Body":
                faceSwapperPanel.SetActive(false);
                bodySwapperPanel.SetActive(true);
                SetBodyPanelActive();
                break;
            default:
                break;
        }
    }

    
}
