using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;
using UnityEngine.U2D.Animation;
using UnityEngine.Networking;
using SimpleFileBrowser;
// using UnityEditor;

public class FileUploadScript : MonoBehaviour
{

    [SerializeField]
    public Swapper pngSwapper;

    #region PNG Importer

    public void OpenExplorerPNG(){
        FileBrowser.ShowLoadDialog( ( paths ) =>{
            string srcPath = paths[0];
            byte[] bytes = FileBrowserHelpers.ReadBytesFromFile(srcPath);
            UploadPNG(srcPath, bytes);
        }, null, FileBrowser.PickMode.Files, false, null, null, "Load", "Select" );
    }

    // public void OpenExplorerPNGEditor(){
    //     string pngPath = EditorUtility.OpenFilePanel("Upload .PNG file", "", "png");
    //     GetPNG(pngPath);
    // }

    // void GetPNG(string path){
    //     if(path != null){
    //         StartCoroutine(UploadPNGEditor(path));
    //     }
    // }

    // Save PNG to unity folder
    void UploadPNG(string path, byte[] srcBytes){
        byte[] bytes = srcBytes;
        string fileName = Path.GetFileName(path);
        string fileName2 = Path.GetFileNameWithoutExtension(path);
        string filePath = Path.Combine(Application.dataPath, fileName);
        // string filePath2 = Path.Combine("PNGs/" + fileName2);
        File.WriteAllBytes(filePath, bytes);
        Debug.Log("File saved to: " + fileName2);
        pngSwapper.LoadUploadedSprite(path, bytes);
    }

    // IEnumerator UploadPNGEditor(string path){
    //     UnityWebRequest www = UnityWebRequest.Get(path);
    //     yield return www.SendWebRequest();

    //     if(www.result == UnityWebRequest.Result.ProtocolError){
    //         Debug.Log(www.error);
    //     }
    //     else{
    //         byte[] bytes = www.downloadHandler.data;
    //         string fileName = Path.GetFileName(path);
    //         string fileName2 = Path.GetFileNameWithoutExtension(path);
    //         string filePath = Path.Combine(Application.dataPath, "Resources/PNGs/" + fileName);
    //         string filePath2 = Path.Combine("PNGs/" + fileName2);
    //         File.WriteAllBytes(filePath, bytes);
    //         Debug.Log("File saved to: " + fileName2);

    //         AssetDatabase.Refresh();
    //         pngSwapper.LoadUploadedSpriteEditor(filePath2, fileName2);
    //     }
    // }

    #endregion
}
