using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MixerSelector : MonoBehaviour{
    #region Inspector

    [SerializeField]
    private TMP_Text titleLabel;

    [SerializeField]
    private Dropdown dropdown;

    [SerializeField]
    private GameObject uploadedSprite;

    #endregion


    #region Methods

    public void Init (MixerCategory category, string[] labels){
        // Set title
        titleLabel.SetText(category.displayTitle);  

        uploadedSprite = GameObject.FindWithTag("UploadedSprite");

        // Populate dropdown
        List<Dropdown.OptionData> spriteLabels = new List<Dropdown.OptionData>();
        foreach ( string label in labels ){
            Dropdown.OptionData data = new Dropdown.OptionData(label);
            spriteLabels.Add(data);
        }

        dropdown.options = spriteLabels;

        // Handle change
        dropdown.onValueChanged.AddListener(optionIndex =>{
            string label = labels[optionIndex];
            category.resolver.SetCategoryAndLabel(category.name, label);
            Debug.Log(uploadedSprite.name);
            if(uploadedSprite.activeSelf){
                uploadedSprite.SetActive(false);
            }
        }); 
    }

    void AddDropdownOption(){
        //Add a new dropdown option
        
    }



    public void AddPNGasOption(){
        dropdown.RefreshShownValue();
    }

    #endregion
}