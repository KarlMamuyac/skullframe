using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIHandlerScript : MonoBehaviour
{
    [SerializeField]
    private GameObject sampleChr;

    [SerializeField]
    private GameObject luchadorChr;

    [SerializeField]
    private GameObject sampleCharPanel;

    [SerializeField]
    private GameObject luchadorSwapperPanel;

    [SerializeField]
    private Button luchadorBtn;

    [SerializeField]
    private Button sampleBtn;

    private Image luchaBtnColor;
    private Image sampleBtnColor;
    private TMP_Text luchaBtnText;
    private TMP_Text sampleBtnText;

    private Color activeBtnColor;
    private Color inactiveBtnColor;
    private Color activeTxtColor;
    private Color inactiveTxtColor;

    public string activeBtnColorHex;
    public string inactiveBtnColorHex;
    public string activeTxtColorHex;
    public string inactiveTxtColorHex;

    void Start(){
        SetUIComponent();
        SetColorParameters();
        SetDefaultPanels();
    }

    void SetDefaultPanels(){
        SetSamplePanelActive();
    }

    void SetUIComponent(){
        luchaBtnColor = luchadorBtn.GetComponent<Image>();
        sampleBtnColor = sampleBtn.GetComponent<Image>();
        luchaBtnText = luchadorBtn.GetComponentInChildren<TMP_Text>();
        sampleBtnText = sampleBtn.GetComponentInChildren<TMP_Text>();
    }

    void SetColorParameters(){
        activeBtnColor = new Color(HexToColor(activeBtnColorHex).r, HexToColor(activeBtnColorHex).g, HexToColor(activeBtnColorHex).b, 1f);
        inactiveBtnColor = new Color(HexToColor(inactiveBtnColorHex).r, HexToColor(inactiveBtnColorHex).g, HexToColor(inactiveBtnColorHex).b, 1f);
        activeTxtColor = new Color(HexToColor(activeTxtColorHex).r, HexToColor(activeTxtColorHex).g, HexToColor(activeTxtColorHex).b, 1f);
        inactiveTxtColor = new Color(HexToColor(inactiveTxtColorHex).r, HexToColor(inactiveTxtColorHex).g, HexToColor(inactiveTxtColorHex).b, 1f);
    }

    Color HexToColor(string hex){
        Color color;
        ColorUtility.TryParseHtmlString(hex, out color);
        return color;
    }

    public void SetLuchaPanelActive(){
        luchaBtnColor.color = activeBtnColor;
        luchaBtnText.color = activeTxtColor;
        sampleBtnColor.color = inactiveBtnColor;
        sampleBtnText.color = inactiveTxtColor;
        sampleCharPanel.SetActive(false);
        luchadorSwapperPanel.SetActive(true);
        sampleChr.SetActive(false);
        luchadorChr.SetActive(true);
    }

    public void SetSamplePanelActive(){
        luchaBtnColor.color = inactiveBtnColor;
        luchaBtnText.color = inactiveTxtColor;
        sampleBtnColor.color = activeBtnColor;
        sampleBtnText.color = activeTxtColor;
        sampleCharPanel.SetActive(true);
        luchadorSwapperPanel.SetActive(false);
        sampleChr.SetActive(true);
        luchadorChr.SetActive(false);
    }

    public void ChangeSwapperPanel(string panelName){
        switch (panelName)
        {
            case "Sample":
                sampleChr.SetActive(true);
                sampleCharPanel.SetActive(true);
                luchadorChr.SetActive(false);
                luchadorSwapperPanel.SetActive(false);
                SetSamplePanelActive();
                break;
            case "Lucha":
                sampleChr.SetActive(false);
                sampleCharPanel.SetActive(false);
                luchadorChr.SetActive(true);
                luchadorSwapperPanel.SetActive(true);
                SetLuchaPanelActive();
                break;
            default:
                break;
        }
    }
   
}
