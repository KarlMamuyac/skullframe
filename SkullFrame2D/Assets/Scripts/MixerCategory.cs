using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public struct MixerCategory{
    #region Fields

        public string name;

        public string displayTitle;

        public UnityEngine.U2D.Animation.SpriteResolver resolver;

    #endregion
}
