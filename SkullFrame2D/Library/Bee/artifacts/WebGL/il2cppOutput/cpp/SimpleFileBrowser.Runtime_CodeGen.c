﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean SimpleFileBrowser.FileBrowser::get_IsOpen()
extern void FileBrowser_get_IsOpen_m18814451BEC8E8CADB599BC8F23E6C37104BE50C (void);
// 0x00000002 System.Void SimpleFileBrowser.FileBrowser::set_IsOpen(System.Boolean)
extern void FileBrowser_set_IsOpen_mC94644F22A61409CE23169AAD18499301862D444 (void);
// 0x00000003 System.Boolean SimpleFileBrowser.FileBrowser::get_Success()
extern void FileBrowser_get_Success_m5809EE68C776F1500213DFFAC167325DC56F879C (void);
// 0x00000004 System.Void SimpleFileBrowser.FileBrowser::set_Success(System.Boolean)
extern void FileBrowser_set_Success_mE3E30E699935AC3FC78C668691DDB21610E1F1B4 (void);
// 0x00000005 System.String[] SimpleFileBrowser.FileBrowser::get_Result()
extern void FileBrowser_get_Result_m68BED71300B0738904437796A2C20ADB8B057FF0 (void);
// 0x00000006 System.Void SimpleFileBrowser.FileBrowser::set_Result(System.String[])
extern void FileBrowser_set_Result_m6656F9251FE3557D72BF0D0F4BA83B475820F8AD (void);
// 0x00000007 SimpleFileBrowser.UISkin SimpleFileBrowser.FileBrowser::get_Skin()
extern void FileBrowser_get_Skin_mF5E5790CDB02A70F885B319526F5FA5C8B4A518F (void);
// 0x00000008 System.Void SimpleFileBrowser.FileBrowser::set_Skin(SimpleFileBrowser.UISkin)
extern void FileBrowser_set_Skin_m66738BFFEE37F4AB8939538A6D4BDA3738EE3916 (void);
// 0x00000009 System.Boolean SimpleFileBrowser.FileBrowser::get_AskPermissions()
extern void FileBrowser_get_AskPermissions_m550ED3B674566CE6C04CB7F76C97B07686BFCA16 (void);
// 0x0000000A System.Void SimpleFileBrowser.FileBrowser::set_AskPermissions(System.Boolean)
extern void FileBrowser_set_AskPermissions_mC267C7AD7FB68495D67E5AF6B9979DEA746A66C9 (void);
// 0x0000000B System.Boolean SimpleFileBrowser.FileBrowser::get_SingleClickMode()
extern void FileBrowser_get_SingleClickMode_mBC1D95A4EF5641CE3D6B8096C609EE0467F03C7D (void);
// 0x0000000C System.Void SimpleFileBrowser.FileBrowser::set_SingleClickMode(System.Boolean)
extern void FileBrowser_set_SingleClickMode_m180E49862F045FAE51B006F800C7EB01E393D29D (void);
// 0x0000000D System.Void SimpleFileBrowser.FileBrowser::add_DisplayedEntriesFilter(SimpleFileBrowser.FileBrowser/FileSystemEntryFilter)
extern void FileBrowser_add_DisplayedEntriesFilter_m7589F78FDCCB84B18E9F961CEE69913F64AB5E26 (void);
// 0x0000000E System.Void SimpleFileBrowser.FileBrowser::remove_DisplayedEntriesFilter(SimpleFileBrowser.FileBrowser/FileSystemEntryFilter)
extern void FileBrowser_remove_DisplayedEntriesFilter_m03ECE3627146AAFFB166553186578B79767CA0F8 (void);
// 0x0000000F System.Single SimpleFileBrowser.FileBrowser::get_DrivesRefreshInterval()
extern void FileBrowser_get_DrivesRefreshInterval_m758A3C4DDE154BD4BDD1C29660FEDB0C5D359E41 (void);
// 0x00000010 System.Void SimpleFileBrowser.FileBrowser::set_DrivesRefreshInterval(System.Single)
extern void FileBrowser_set_DrivesRefreshInterval_m272B5E8EB171918BAF26302F29847A276284EE3F (void);
// 0x00000011 System.Boolean SimpleFileBrowser.FileBrowser::get_ShowHiddenFiles()
extern void FileBrowser_get_ShowHiddenFiles_m56B300897CACD137E41BA49D4966B0D5ACCF6D60 (void);
// 0x00000012 System.Void SimpleFileBrowser.FileBrowser::set_ShowHiddenFiles(System.Boolean)
extern void FileBrowser_set_ShowHiddenFiles_mAFC629F52C31CA3C826D1699C1A32F8D7C03D57D (void);
// 0x00000013 System.Boolean SimpleFileBrowser.FileBrowser::get_DisplayHiddenFilesToggle()
extern void FileBrowser_get_DisplayHiddenFilesToggle_mF2F1261B15DF7A8A9467CBA2D9A4D9CF7E21B1C0 (void);
// 0x00000014 System.Void SimpleFileBrowser.FileBrowser::set_DisplayHiddenFilesToggle(System.Boolean)
extern void FileBrowser_set_DisplayHiddenFilesToggle_mE7BDB07034AC4C92162E4CF763F4DA9DC683ED3A (void);
// 0x00000015 System.String SimpleFileBrowser.FileBrowser::get_AllFilesFilterText()
extern void FileBrowser_get_AllFilesFilterText_m11A59EC77A38E7954AD443B9BC9B445468BDB842 (void);
// 0x00000016 System.Void SimpleFileBrowser.FileBrowser::set_AllFilesFilterText(System.String)
extern void FileBrowser_set_AllFilesFilterText_mDE2F45DE0DE0CE7F7074ABE3CB39180485B215E8 (void);
// 0x00000017 System.String SimpleFileBrowser.FileBrowser::get_FoldersFilterText()
extern void FileBrowser_get_FoldersFilterText_mA3D49D59F8F4FB8D7DD400B783872F0505785FA5 (void);
// 0x00000018 System.Void SimpleFileBrowser.FileBrowser::set_FoldersFilterText(System.String)
extern void FileBrowser_set_FoldersFilterText_m159DFA629E9C095D04F97A11A5EE79B87D021BA0 (void);
// 0x00000019 System.String SimpleFileBrowser.FileBrowser::get_PickFolderQuickLinkText()
extern void FileBrowser_get_PickFolderQuickLinkText_mBEFAA93F33C6344716A60C7DFDD4B5752AE7CBEF (void);
// 0x0000001A System.Void SimpleFileBrowser.FileBrowser::set_PickFolderQuickLinkText(System.String)
extern void FileBrowser_set_PickFolderQuickLinkText_m5C463A1BF7E16C97302082D49C87176EFC6CFF6D (void);
// 0x0000001B SimpleFileBrowser.FileBrowser SimpleFileBrowser.FileBrowser::get_Instance()
extern void FileBrowser_get_Instance_mB99D0C8AFF82F3377E62304CFB0C9210F2710DD4 (void);
// 0x0000001C System.Boolean SimpleFileBrowser.FileBrowser::get_AllExtensionsHaveSingleSuffix()
extern void FileBrowser_get_AllExtensionsHaveSingleSuffix_mE1B6D3A19497EB0952FAC108AA6DCD46A63AC3A2 (void);
// 0x0000001D System.String SimpleFileBrowser.FileBrowser::get_CurrentPath()
extern void FileBrowser_get_CurrentPath_mBFA8E36877B8A5F0817EADA032344219B8207E4F (void);
// 0x0000001E System.Void SimpleFileBrowser.FileBrowser::set_CurrentPath(System.String)
extern void FileBrowser_set_CurrentPath_mC77AFECC7927FE02B265B490C3825EEE685E3BE7 (void);
// 0x0000001F System.String SimpleFileBrowser.FileBrowser::get_SearchString()
extern void FileBrowser_get_SearchString_mE8FD1C23E5E06B45B4A3E0082ECC671E7F186EF6 (void);
// 0x00000020 System.Void SimpleFileBrowser.FileBrowser::set_SearchString(System.String)
extern void FileBrowser_set_SearchString_mDDD0C4D50DA3999E4338C0A8A001279D08331E48 (void);
// 0x00000021 System.Boolean SimpleFileBrowser.FileBrowser::get_AcceptNonExistingFilename()
extern void FileBrowser_get_AcceptNonExistingFilename_mA1E6CAA75D40889BEAA0B528C86DBBF10841913F (void);
// 0x00000022 System.Void SimpleFileBrowser.FileBrowser::set_AcceptNonExistingFilename(System.Boolean)
extern void FileBrowser_set_AcceptNonExistingFilename_mF0E53CE81D674BEBE2583049BEE0586661374079 (void);
// 0x00000023 SimpleFileBrowser.FileBrowser/PickMode SimpleFileBrowser.FileBrowser::get_PickerMode()
extern void FileBrowser_get_PickerMode_m1E7F1A135BE57C9B4E56097009BC001E2CCDF616 (void);
// 0x00000024 System.Void SimpleFileBrowser.FileBrowser::set_PickerMode(SimpleFileBrowser.FileBrowser/PickMode)
extern void FileBrowser_set_PickerMode_m252A1D099DEF70FAF8AC72FD5D7A4C45B65AA206 (void);
// 0x00000025 System.Boolean SimpleFileBrowser.FileBrowser::get_AllowMultiSelection()
extern void FileBrowser_get_AllowMultiSelection_m3CE75D1DD5A555180373AFE8E446D298F9CCB97C (void);
// 0x00000026 System.Void SimpleFileBrowser.FileBrowser::set_AllowMultiSelection(System.Boolean)
extern void FileBrowser_set_AllowMultiSelection_m4FC44DD7C7836F9F094C0CBE960A29259B0D1E05 (void);
// 0x00000027 System.Boolean SimpleFileBrowser.FileBrowser::get_MultiSelectionToggleSelectionMode()
extern void FileBrowser_get_MultiSelectionToggleSelectionMode_m0CC81C607E6309B5E0E706E5A8F74CF3E541A082 (void);
// 0x00000028 System.Void SimpleFileBrowser.FileBrowser::set_MultiSelectionToggleSelectionMode(System.Boolean)
extern void FileBrowser_set_MultiSelectionToggleSelectionMode_m0F58D2FE698D3B849FB78DC405551C2A54899830 (void);
// 0x00000029 System.String SimpleFileBrowser.FileBrowser::get_Title()
extern void FileBrowser_get_Title_mB4AB85A4B858B6CE7671AE32D30D7EE997CEA8B3 (void);
// 0x0000002A System.Void SimpleFileBrowser.FileBrowser::set_Title(System.String)
extern void FileBrowser_set_Title_mA303A37D55D2D07C43D734B10507A42B55D10423 (void);
// 0x0000002B System.String SimpleFileBrowser.FileBrowser::get_SubmitButtonText()
extern void FileBrowser_get_SubmitButtonText_m4E82C1110DB829DD32BA479A3D5337D1659D2D0C (void);
// 0x0000002C System.Void SimpleFileBrowser.FileBrowser::set_SubmitButtonText(System.String)
extern void FileBrowser_set_SubmitButtonText_m29CFCD8B22FB378B3E8F4388C72C7A1A7AB3C0D4 (void);
// 0x0000002D System.String SimpleFileBrowser.FileBrowser::get_LastBrowsedFolder()
extern void FileBrowser_get_LastBrowsedFolder_m6457C65E0801437674EF2AC7C3732368ACB70438 (void);
// 0x0000002E System.Void SimpleFileBrowser.FileBrowser::set_LastBrowsedFolder(System.String)
extern void FileBrowser_set_LastBrowsedFolder_m74581F03A4ADFF9A990FD5F467A1E8978DF7A46A (void);
// 0x0000002F System.Void SimpleFileBrowser.FileBrowser::Awake()
extern void FileBrowser_Awake_mE271079885CA728F72B3F73D83CE78DC5880C23A (void);
// 0x00000030 System.Void SimpleFileBrowser.FileBrowser::OnRectTransformDimensionsChange()
extern void FileBrowser_OnRectTransformDimensionsChange_mE4DAC20591CBB27C5A5A2F97A66E86FCB931A9B3 (void);
// 0x00000031 System.Void SimpleFileBrowser.FileBrowser::Update()
extern void FileBrowser_Update_m3F1F142729B656AC489C8AA7307E174CBC54C565 (void);
// 0x00000032 System.Void SimpleFileBrowser.FileBrowser::LateUpdate()
extern void FileBrowser_LateUpdate_mA3374004B750A9D6FF0F4D11752DF0FC88B32235 (void);
// 0x00000033 System.Void SimpleFileBrowser.FileBrowser::OnApplicationFocus(System.Boolean)
extern void FileBrowser_OnApplicationFocus_m0D8C1F78585868840DDE556454B4D22A766E861E (void);
// 0x00000034 SimpleFileBrowser.OnItemClickedHandler SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.get_OnItemClicked()
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_get_OnItemClicked_m9126447B8863E925FD5D3B2CF60188BD993FC9F5 (void);
// 0x00000035 System.Void SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.set_OnItemClicked(SimpleFileBrowser.OnItemClickedHandler)
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_set_OnItemClicked_m202B2600CCE6D8BC02F6CD6929C065B0F2EFF270 (void);
// 0x00000036 System.Int32 SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.get_Count()
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_get_Count_m0AC6A73A9EBA0573CFABED865682EF679CF79D95 (void);
// 0x00000037 System.Single SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.get_ItemHeight()
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_get_ItemHeight_m0577A36A99AE4760ADCE32284B245268909CC2C1 (void);
// 0x00000038 SimpleFileBrowser.ListItem SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.CreateItem()
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_CreateItem_m3A94D4404F711861B1A9CE82DD08DC27B239D09A (void);
// 0x00000039 System.Void SimpleFileBrowser.FileBrowser::SimpleFileBrowser.IListViewAdapter.SetItemContent(SimpleFileBrowser.ListItem)
extern void FileBrowser_SimpleFileBrowser_IListViewAdapter_SetItemContent_mF1D94F26886A961DEAA8528D76625CAEF0812F9A (void);
// 0x0000003A System.Void SimpleFileBrowser.FileBrowser::InitializeQuickLinks()
extern void FileBrowser_InitializeQuickLinks_mC08CA308D9034BA498A4CE1AC3FFE849230DFB56 (void);
// 0x0000003B System.Void SimpleFileBrowser.FileBrowser::RefreshDriveQuickLinks()
extern void FileBrowser_RefreshDriveQuickLinks_m540D5683EDD68826961F7814C499450EB2E14798 (void);
// 0x0000003C System.Void SimpleFileBrowser.FileBrowser::RefreshSkin()
extern void FileBrowser_RefreshSkin_mB4CF280DFD76B5963B1957FDC4C5D1C7630606DC (void);
// 0x0000003D System.Void SimpleFileBrowser.FileBrowser::OnBackButtonPressed()
extern void FileBrowser_OnBackButtonPressed_mAD0E3CAB0818551D542A9516897E1B1496B44968 (void);
// 0x0000003E System.Void SimpleFileBrowser.FileBrowser::OnForwardButtonPressed()
extern void FileBrowser_OnForwardButtonPressed_mFE46E8ECD432849A423C848FB7D39328CB87EC62 (void);
// 0x0000003F System.Void SimpleFileBrowser.FileBrowser::OnUpButtonPressed()
extern void FileBrowser_OnUpButtonPressed_mCAA26ECCA4B13284A4B7E4D977A388ACCF1AFB9E (void);
// 0x00000040 System.Void SimpleFileBrowser.FileBrowser::OnMoreOptionsButtonClicked()
extern void FileBrowser_OnMoreOptionsButtonClicked_m10E401F7C1C6F202F1848A96DA25AECFA6D949C7 (void);
// 0x00000041 System.Void SimpleFileBrowser.FileBrowser::OnContextMenuTriggered(UnityEngine.Vector2)
extern void FileBrowser_OnContextMenuTriggered_mFA0AA158FE9AE9AD8ECB0D3DEB8D169481E02319 (void);
// 0x00000042 System.Void SimpleFileBrowser.FileBrowser::ShowContextMenuAt(UnityEngine.Vector2,System.Boolean)
extern void FileBrowser_ShowContextMenuAt_m13F6C5767CB0B266FF2F2D583C23957BFC74453C (void);
// 0x00000043 System.Void SimpleFileBrowser.FileBrowser::OnSubmitButtonClicked()
extern void FileBrowser_OnSubmitButtonClicked_m3332294C34AC603145DA335F0C86BD11A0688DF8 (void);
// 0x00000044 System.Void SimpleFileBrowser.FileBrowser::OnCancelButtonClicked()
extern void FileBrowser_OnCancelButtonClicked_m48955553F8A90376E93A23112E8ECCCC2CD82954 (void);
// 0x00000045 System.Void SimpleFileBrowser.FileBrowser::OnOperationSuccessful(System.String[])
extern void FileBrowser_OnOperationSuccessful_m2CF8A90FC6BA08A1765667852A41111CF43FB4DB (void);
// 0x00000046 System.Void SimpleFileBrowser.FileBrowser::OnOperationCanceled(System.Boolean)
extern void FileBrowser_OnOperationCanceled_mDC4CDB908EE1BBC64F34DC02D56BF6B9746F78D8 (void);
// 0x00000047 System.Void SimpleFileBrowser.FileBrowser::OnPathChanged(System.String)
extern void FileBrowser_OnPathChanged_mA276DEEBF8BF2572D763A15AA21036216B996661 (void);
// 0x00000048 System.Void SimpleFileBrowser.FileBrowser::OnSearchStringChanged(System.String)
extern void FileBrowser_OnSearchStringChanged_m06A1A24E20B3BA543E3F644CFE49D682AF810BA2 (void);
// 0x00000049 System.Void SimpleFileBrowser.FileBrowser::OnFilterChanged()
extern void FileBrowser_OnFilterChanged_m04F962445E850063CD19D87F1DED4B9962BCC0A7 (void);
// 0x0000004A System.Void SimpleFileBrowser.FileBrowser::OnShowHiddenFilesToggleChanged()
extern void FileBrowser_OnShowHiddenFilesToggleChanged_m72833F478BA724C4D32C89EC1AD44D1F9D1C478F (void);
// 0x0000004B System.Void SimpleFileBrowser.FileBrowser::OnItemSelected(SimpleFileBrowser.FileBrowserItem,System.Boolean)
extern void FileBrowser_OnItemSelected_m553F21D12D4F28911E2B04E2B5E14D434AE8B067 (void);
// 0x0000004C System.Void SimpleFileBrowser.FileBrowser::OnItemHeld(SimpleFileBrowser.FileBrowserItem)
extern void FileBrowser_OnItemHeld_m4616B0F779C71648811095E004E6852065EC8F70 (void);
// 0x0000004D System.Char SimpleFileBrowser.FileBrowser::OnValidateFilenameInput(System.String,System.Int32,System.Char)
extern void FileBrowser_OnValidateFilenameInput_m12AF80C6D178EDAD4462D1F8E4AA4FA03D79D02A (void);
// 0x0000004E System.Void SimpleFileBrowser.FileBrowser::OnFilenameInputChanged(System.String)
extern void FileBrowser_OnFilenameInputChanged_m03FC49F2400775036127FF42D4E27568809CE1B7 (void);
// 0x0000004F System.Void SimpleFileBrowser.FileBrowser::Show(System.String,System.String)
extern void FileBrowser_Show_m64A9C6492FE4F3A2E083847F85078AF64A491C98 (void);
// 0x00000050 System.Void SimpleFileBrowser.FileBrowser::Hide()
extern void FileBrowser_Hide_mD14D207CDCAEA3EBCE749396FA71BF049BC897A1 (void);
// 0x00000051 System.Void SimpleFileBrowser.FileBrowser::RefreshFiles(System.Boolean)
extern void FileBrowser_RefreshFiles_m14E4ABD98486CB7DDAD461D233295B6DA37ED763 (void);
// 0x00000052 System.Void SimpleFileBrowser.FileBrowser::SelectAllFiles()
extern void FileBrowser_SelectAllFiles_m44CC4BEA6B91DF34E635049598E4E5FFFF45E8B1 (void);
// 0x00000053 System.Void SimpleFileBrowser.FileBrowser::DeselectAllFiles()
extern void FileBrowser_DeselectAllFiles_m77712CDD088C8976A9D19FDE678E6712C4BB3AA4 (void);
// 0x00000054 System.Void SimpleFileBrowser.FileBrowser::CreateNewFolder()
extern void FileBrowser_CreateNewFolder_m134E6B000CC231D49ADE213F46A4FC0B43C63E43 (void);
// 0x00000055 System.Collections.IEnumerator SimpleFileBrowser.FileBrowser::CreateNewFolderCoroutine()
extern void FileBrowser_CreateNewFolderCoroutine_mB8A66589D4DCF5D8ECA7C2EA681101BD9D17CABD (void);
// 0x00000056 System.Void SimpleFileBrowser.FileBrowser::RenameSelectedFile()
extern void FileBrowser_RenameSelectedFile_m81CA56F0C89D20230E7A22C487D7B914E45582CF (void);
// 0x00000057 System.Void SimpleFileBrowser.FileBrowser::DeleteSelectedFiles()
extern void FileBrowser_DeleteSelectedFiles_mCA7847F4540F0B4DBEF0885A3C692F9F0A0562FE (void);
// 0x00000058 System.Void SimpleFileBrowser.FileBrowser::PersistFileEntrySelection()
extern void FileBrowser_PersistFileEntrySelection_m539A9413D6681B3BA6FE5A474F255CD1F282CA93 (void);
// 0x00000059 System.Boolean SimpleFileBrowser.FileBrowser::AddQuickLink(UnityEngine.Sprite,System.String,System.String)
extern void FileBrowser_AddQuickLink_mF254B02358535E14D2319F1AC1230F513D58B42A (void);
// 0x0000005A System.Void SimpleFileBrowser.FileBrowser::ClearQuickLinksInternal()
extern void FileBrowser_ClearQuickLinksInternal_m16606512E3CA993A619C864CC764D9253FBAE162 (void);
// 0x0000005B System.Void SimpleFileBrowser.FileBrowser::EnsureScrollViewIsWithinBounds()
extern void FileBrowser_EnsureScrollViewIsWithinBounds_mB014E75CA8EFC7FC5B9A388413D610BE6747D954 (void);
// 0x0000005C System.Void SimpleFileBrowser.FileBrowser::EnsureWindowIsWithinBounds()
extern void FileBrowser_EnsureWindowIsWithinBounds_m6052BE3802013AF0A0F003D19C6A132E144F3EE6 (void);
// 0x0000005D System.Void SimpleFileBrowser.FileBrowser::OnWindowDimensionsChanged(UnityEngine.Vector2)
extern void FileBrowser_OnWindowDimensionsChanged_mF3F9FE60AFAAD85235F6AF5E5F6E28530C397A0C (void);
// 0x0000005E UnityEngine.Sprite SimpleFileBrowser.FileBrowser::GetIconForFileEntry(SimpleFileBrowser.FileSystemEntry)
extern void FileBrowser_GetIconForFileEntry_m39E97FDC81C7D26CDC1A09146CCB8D50949FD147 (void);
// 0x0000005F System.String SimpleFileBrowser.FileBrowser::GetExtensionFromFilename(System.String,System.Boolean)
extern void FileBrowser_GetExtensionFromFilename_mDFAD3BC2729A0685EDCD8D46085C755670E02346 (void);
// 0x00000060 System.String SimpleFileBrowser.FileBrowser::GetPathWithoutTrailingDirectorySeparator(System.String)
extern void FileBrowser_GetPathWithoutTrailingDirectorySeparator_m56DF6CC3E788CE233690E42B4878B6D45E5F4B21 (void);
// 0x00000061 System.Void SimpleFileBrowser.FileBrowser::UpdateFilenameInputFieldWithSelection()
extern void FileBrowser_UpdateFilenameInputFieldWithSelection_mE68C8FA9B60ADB869F43D6FD7D8ABA5301A7F7E1 (void);
// 0x00000062 System.Int32 SimpleFileBrowser.FileBrowser::ExtractFilenameFromInput(System.String,System.Int32&,System.Int32&)
extern void FileBrowser_ExtractFilenameFromInput_m8D87F77E24E8BCCC89D886486CAE777A7848FADD (void);
// 0x00000063 System.Int32 SimpleFileBrowser.FileBrowser::FilenameInputToFileEntryIndex(System.String,System.Int32,System.Int32)
extern void FileBrowser_FilenameInputToFileEntryIndex_m8EEFD8C4E92482B7D67428C5B2719E9397A9F03E (void);
// 0x00000064 System.Boolean SimpleFileBrowser.FileBrowser::VerifyFilenameInput(System.String,System.Int32,System.Int32)
extern void FileBrowser_VerifyFilenameInput_m8533205E3B8F0854F81075FBE36C1705CEDFEEF5 (void);
// 0x00000065 System.Int32 SimpleFileBrowser.FileBrowser::CalculateLengthOfDropdownText(System.String)
extern void FileBrowser_CalculateLengthOfDropdownText_m9066BB1F5AB9C6051690469BD3FFA52BC20ADD9D (void);
// 0x00000066 System.String SimpleFileBrowser.FileBrowser::GetInitialPath(System.String)
extern void FileBrowser_GetInitialPath_mB6A20DD2C4B53CA447385B11A12FDA952FFB5831 (void);
// 0x00000067 System.Boolean SimpleFileBrowser.FileBrowser::ShowSaveDialog(SimpleFileBrowser.FileBrowser/OnSuccess,SimpleFileBrowser.FileBrowser/OnCancel,SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_ShowSaveDialog_m144B1D73495975776018A3D243D9C5902CAA8B2C (void);
// 0x00000068 System.Boolean SimpleFileBrowser.FileBrowser::ShowLoadDialog(SimpleFileBrowser.FileBrowser/OnSuccess,SimpleFileBrowser.FileBrowser/OnCancel,SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_ShowLoadDialog_mAE177552745B54980984B3EBE1EB9830B79DC082 (void);
// 0x00000069 System.Boolean SimpleFileBrowser.FileBrowser::ShowDialogInternal(SimpleFileBrowser.FileBrowser/OnSuccess,SimpleFileBrowser.FileBrowser/OnCancel,SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_ShowDialogInternal_mC92646F8C1B0FF0AD77C57F9D0EBEEDE2BB07ADE (void);
// 0x0000006A System.Void SimpleFileBrowser.FileBrowser::HideDialog(System.Boolean)
extern void FileBrowser_HideDialog_m02BE36614EF3CEFE0F3D41E44EBA950427055811 (void);
// 0x0000006B System.Collections.IEnumerator SimpleFileBrowser.FileBrowser::WaitForSaveDialog(SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_WaitForSaveDialog_m3190752768E451A01CAE16E47FAFE4E531E96EA2 (void);
// 0x0000006C System.Collections.IEnumerator SimpleFileBrowser.FileBrowser::WaitForLoadDialog(SimpleFileBrowser.FileBrowser/PickMode,System.Boolean,System.String,System.String,System.String,System.String)
extern void FileBrowser_WaitForLoadDialog_m5ABC9FC7FDD5C18B32153309C0204E5862C584B9 (void);
// 0x0000006D System.Boolean SimpleFileBrowser.FileBrowser::AddQuickLink(System.String,System.String,UnityEngine.Sprite)
extern void FileBrowser_AddQuickLink_m5FA47D097233AC295433BC44263C91A4ED6F29B9 (void);
// 0x0000006E System.Void SimpleFileBrowser.FileBrowser::ClearQuickLinks()
extern void FileBrowser_ClearQuickLinks_m30154506F3DA9285139828125C0C8EDC84415462 (void);
// 0x0000006F System.Void SimpleFileBrowser.FileBrowser::SetExcludedExtensions(System.String[])
extern void FileBrowser_SetExcludedExtensions_mF4555FEA87743C688BF75C4A90B2CC9BB80547A5 (void);
// 0x00000070 System.Void SimpleFileBrowser.FileBrowser::SetFilters(System.Boolean)
extern void FileBrowser_SetFilters_m65982A252AE3CB8F23BA581A64D017E4198AABD8 (void);
// 0x00000071 System.Void SimpleFileBrowser.FileBrowser::SetFilters(System.Boolean,System.Collections.Generic.IEnumerable`1<System.String>)
extern void FileBrowser_SetFilters_m9241687743642DA21496022FF91E789BF00AB257 (void);
// 0x00000072 System.Void SimpleFileBrowser.FileBrowser::SetFilters(System.Boolean,System.String[])
extern void FileBrowser_SetFilters_m9A1F7D8B946BE18B89EC1FEBE09E68C227EEF5A0 (void);
// 0x00000073 System.Void SimpleFileBrowser.FileBrowser::SetFilters(System.Boolean,System.Collections.Generic.IEnumerable`1<SimpleFileBrowser.FileBrowser/Filter>)
extern void FileBrowser_SetFilters_mE9AC44A5A36311CBB2A367869FC01C5E62E2001E (void);
// 0x00000074 System.Void SimpleFileBrowser.FileBrowser::SetFilters(System.Boolean,SimpleFileBrowser.FileBrowser/Filter[])
extern void FileBrowser_SetFilters_mDAFF9AD6984CF9B471A02570ABAE4967A7C70853 (void);
// 0x00000075 System.Void SimpleFileBrowser.FileBrowser::SetFiltersPreProcessing(System.Boolean)
extern void FileBrowser_SetFiltersPreProcessing_m06AA25B9394BC9012763EA01B0EB166C8BDBEE42 (void);
// 0x00000076 System.Void SimpleFileBrowser.FileBrowser::SetFiltersPostProcessing()
extern void FileBrowser_SetFiltersPostProcessing_m026D4142F0AACA8173F861F5A0E86969EBB01C7C (void);
// 0x00000077 System.Boolean SimpleFileBrowser.FileBrowser::SetDefaultFilter(System.String)
extern void FileBrowser_SetDefaultFilter_m85E9F64B38B73DD95159420FFF396D50C2084B2D (void);
// 0x00000078 SimpleFileBrowser.FileBrowser/Permission SimpleFileBrowser.FileBrowser::CheckPermission()
extern void FileBrowser_CheckPermission_mB59E9671407F61B85EA45F21A7F69C289E7FB7B4 (void);
// 0x00000079 SimpleFileBrowser.FileBrowser/Permission SimpleFileBrowser.FileBrowser::RequestPermission()
extern void FileBrowser_RequestPermission_m7F419FBC909B5904989D292C92427ACB414B489E (void);
// 0x0000007A System.Void SimpleFileBrowser.FileBrowser::.ctor()
extern void FileBrowser__ctor_mA14C08F72ED0CDE1E8DDDD71EE3194A89AA4A5D5 (void);
// 0x0000007B System.Void SimpleFileBrowser.FileBrowser::.cctor()
extern void FileBrowser__cctor_m8B60B9C9A649551158D47C24E68D2225BA5CBC88 (void);
// 0x0000007C System.Void SimpleFileBrowser.FileBrowser::<CreateNewFolderCoroutine>b__225_0(System.String)
extern void FileBrowser_U3CCreateNewFolderCoroutineU3Eb__225_0_mB6EC002D40146C5504A129B8A05B3CF47C0A58E8 (void);
// 0x0000007D System.Void SimpleFileBrowser.FileBrowser::<DeleteSelectedFiles>b__227_0()
extern void FileBrowser_U3CDeleteSelectedFilesU3Eb__227_0_m32D69DCD61D56C2D0D1ABAA04FE41E4D358FC742 (void);
// 0x0000007E System.Void SimpleFileBrowser.FileBrowser/Filter::.ctor(System.String)
extern void Filter__ctor_m1A2EDFD7D15168A8C473A0F6760D6643200DCDBE (void);
// 0x0000007F System.Void SimpleFileBrowser.FileBrowser/Filter::.ctor(System.String,System.String)
extern void Filter__ctor_m26233F843105D6920F34365B6BD6B82BFA9FB44E (void);
// 0x00000080 System.Void SimpleFileBrowser.FileBrowser/Filter::.ctor(System.String,System.String[])
extern void Filter__ctor_m0082D963A2376632C2CEAC00B337B8133F53FD29 (void);
// 0x00000081 System.Boolean SimpleFileBrowser.FileBrowser/Filter::MatchesExtension(System.String,System.Boolean)
extern void Filter_MatchesExtension_m599F1975211ADE2667EC0B6AE513A1451153042E (void);
// 0x00000082 System.String SimpleFileBrowser.FileBrowser/Filter::ToString()
extern void Filter_ToString_mDE0DACA728B5CAFBC6C1828D9BCF81440700728E (void);
// 0x00000083 System.Void SimpleFileBrowser.FileBrowser/OnSuccess::.ctor(System.Object,System.IntPtr)
extern void OnSuccess__ctor_mAED1943D7906AE017191AFC0DC639FCD5B016D25 (void);
// 0x00000084 System.Void SimpleFileBrowser.FileBrowser/OnSuccess::Invoke(System.String[])
extern void OnSuccess_Invoke_m4067B92437B334D4939C8A18956DB1855DAD060B (void);
// 0x00000085 System.IAsyncResult SimpleFileBrowser.FileBrowser/OnSuccess::BeginInvoke(System.String[],System.AsyncCallback,System.Object)
extern void OnSuccess_BeginInvoke_mC2A00A176DF5240F86FFF69E73BCBCAE4A3641E2 (void);
// 0x00000086 System.Void SimpleFileBrowser.FileBrowser/OnSuccess::EndInvoke(System.IAsyncResult)
extern void OnSuccess_EndInvoke_mFBB23F394B914823AEBF9ABA3FA0490A12305CE1 (void);
// 0x00000087 System.Void SimpleFileBrowser.FileBrowser/OnCancel::.ctor(System.Object,System.IntPtr)
extern void OnCancel__ctor_m26ACFA25E1D02F85A27B166AF42CFCFBFFD0772C (void);
// 0x00000088 System.Void SimpleFileBrowser.FileBrowser/OnCancel::Invoke()
extern void OnCancel_Invoke_mE143A09B94A5BA1EC2E849F9442BA582C0A74361 (void);
// 0x00000089 System.IAsyncResult SimpleFileBrowser.FileBrowser/OnCancel::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnCancel_BeginInvoke_mE5AFDA66A648E987B9AF356AF0535A93F31FF8FA (void);
// 0x0000008A System.Void SimpleFileBrowser.FileBrowser/OnCancel::EndInvoke(System.IAsyncResult)
extern void OnCancel_EndInvoke_m3E2A8577E6F859560F80CEB507DB1A73A9B8987D (void);
// 0x0000008B System.Void SimpleFileBrowser.FileBrowser/FileSystemEntryFilter::.ctor(System.Object,System.IntPtr)
extern void FileSystemEntryFilter__ctor_m3869D23A38762C2C87A6B728CFEBBEADB8FF817E (void);
// 0x0000008C System.Boolean SimpleFileBrowser.FileBrowser/FileSystemEntryFilter::Invoke(SimpleFileBrowser.FileSystemEntry)
extern void FileSystemEntryFilter_Invoke_m9516A832F92C0BC36EBFC9EB938002D9663B1ABA (void);
// 0x0000008D System.IAsyncResult SimpleFileBrowser.FileBrowser/FileSystemEntryFilter::BeginInvoke(SimpleFileBrowser.FileSystemEntry,System.AsyncCallback,System.Object)
extern void FileSystemEntryFilter_BeginInvoke_m705E174CB6DADA9AEB3174922D9B32752F12489B (void);
// 0x0000008E System.Boolean SimpleFileBrowser.FileBrowser/FileSystemEntryFilter::EndInvoke(System.IAsyncResult)
extern void FileSystemEntryFilter_EndInvoke_m31005911F9DC628C5979B063494F67830D48D611 (void);
// 0x0000008F System.Void SimpleFileBrowser.FileBrowser/<>c::.cctor()
extern void U3CU3Ec__cctor_m907FDC5153760B702813F89A1403512782C27FCD (void);
// 0x00000090 System.Void SimpleFileBrowser.FileBrowser/<>c::.ctor()
extern void U3CU3Ec__ctor_mDC67BB8C9A0B1001B8C37486C2AA5D9197175C1A (void);
// 0x00000091 System.Int32 SimpleFileBrowser.FileBrowser/<>c::<RefreshFiles>b__221_0(SimpleFileBrowser.FileSystemEntry,SimpleFileBrowser.FileSystemEntry)
extern void U3CU3Ec_U3CRefreshFilesU3Eb__221_0_m7F4E253E3CC966C2CF3324DEE33C8222202518EA (void);
// 0x00000092 System.Void SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__225::.ctor(System.Int32)
extern void U3CCreateNewFolderCoroutineU3Ed__225__ctor_m67A83A599B1CA7186A4E09F28F8D02D33FBFE9CD (void);
// 0x00000093 System.Void SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__225::System.IDisposable.Dispose()
extern void U3CCreateNewFolderCoroutineU3Ed__225_System_IDisposable_Dispose_m44BE0222F4CACCAE308A6D9CAF1E3D644A31E088 (void);
// 0x00000094 System.Boolean SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__225::MoveNext()
extern void U3CCreateNewFolderCoroutineU3Ed__225_MoveNext_m7BAFA8B04EE2B6E82B64B51FE419A0CF99A21478 (void);
// 0x00000095 System.Object SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__225::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateNewFolderCoroutineU3Ed__225_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD7D6930A8458B2785DF415AF2491A582AA8E42D3 (void);
// 0x00000096 System.Void SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__225::System.Collections.IEnumerator.Reset()
extern void U3CCreateNewFolderCoroutineU3Ed__225_System_Collections_IEnumerator_Reset_m631BB4979996667E05FCDD3758DB586026B69946 (void);
// 0x00000097 System.Object SimpleFileBrowser.FileBrowser/<CreateNewFolderCoroutine>d__225::System.Collections.IEnumerator.get_Current()
extern void U3CCreateNewFolderCoroutineU3Ed__225_System_Collections_IEnumerator_get_Current_m0434987E41C9DB04860419E9DD9EE7941522918E (void);
// 0x00000098 System.Void SimpleFileBrowser.FileBrowser/<>c__DisplayClass226_0::.ctor()
extern void U3CU3Ec__DisplayClass226_0__ctor_mE630ED777201A655FB478F9CD988B0C8883B2040 (void);
// 0x00000099 System.Void SimpleFileBrowser.FileBrowser/<>c__DisplayClass226_0::<RenameSelectedFile>b__0(System.String)
extern void U3CU3Ec__DisplayClass226_0_U3CRenameSelectedFileU3Eb__0_mE665142043B8931B13FC1F0C274813774EDFB86B (void);
// 0x0000009A System.Void SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__247::.ctor(System.Int32)
extern void U3CWaitForSaveDialogU3Ed__247__ctor_m495B2CF322F9A4C6C7C46B34BB6A86E1CB8AB39C (void);
// 0x0000009B System.Void SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__247::System.IDisposable.Dispose()
extern void U3CWaitForSaveDialogU3Ed__247_System_IDisposable_Dispose_mBAC79E1622A6000A1364A57AF2590C98D24C55D1 (void);
// 0x0000009C System.Boolean SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__247::MoveNext()
extern void U3CWaitForSaveDialogU3Ed__247_MoveNext_m5780A6EB2FF85EEACCA951DB9F7089BEF266F877 (void);
// 0x0000009D System.Object SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__247::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSaveDialogU3Ed__247_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m391D053B3B9F791F26CABB1F0F8617663C197226 (void);
// 0x0000009E System.Void SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__247::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSaveDialogU3Ed__247_System_Collections_IEnumerator_Reset_m6FEC308614348E262E8DA756DA8F12A00CD00286 (void);
// 0x0000009F System.Object SimpleFileBrowser.FileBrowser/<WaitForSaveDialog>d__247::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSaveDialogU3Ed__247_System_Collections_IEnumerator_get_Current_m9661EBF33B737CE78BC0EF76F15B83A1FE80FF42 (void);
// 0x000000A0 System.Void SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__248::.ctor(System.Int32)
extern void U3CWaitForLoadDialogU3Ed__248__ctor_m1D1695204E2EA535969FBD490ECA046E08044AB2 (void);
// 0x000000A1 System.Void SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__248::System.IDisposable.Dispose()
extern void U3CWaitForLoadDialogU3Ed__248_System_IDisposable_Dispose_m81CD0AFC6D088F80A7E858125D0E821772252300 (void);
// 0x000000A2 System.Boolean SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__248::MoveNext()
extern void U3CWaitForLoadDialogU3Ed__248_MoveNext_m146AAAF1BE30DF27CD8099CD057F7734D70EDBE7 (void);
// 0x000000A3 System.Object SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__248::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForLoadDialogU3Ed__248_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1752A1CEA7D63E5E11485504CF1FFC7DCDC0EE8 (void);
// 0x000000A4 System.Void SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__248::System.Collections.IEnumerator.Reset()
extern void U3CWaitForLoadDialogU3Ed__248_System_Collections_IEnumerator_Reset_mEE6C5A4B06D6FF06A180A53E7AE171B00566BD2F (void);
// 0x000000A5 System.Object SimpleFileBrowser.FileBrowser/<WaitForLoadDialog>d__248::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForLoadDialogU3Ed__248_System_Collections_IEnumerator_get_Current_m839B958CDCD2A6B0C33718FCA76B880C082C27C7 (void);
// 0x000000A6 System.Void SimpleFileBrowser.FileBrowserContextMenu::Show(System.Boolean,System.Boolean,System.Boolean,System.Boolean,UnityEngine.Vector2,System.Boolean)
extern void FileBrowserContextMenu_Show_mA733F9163F6D4929AF02FF90393B092D6A1BDCD8 (void);
// 0x000000A7 System.Void SimpleFileBrowser.FileBrowserContextMenu::Hide()
extern void FileBrowserContextMenu_Hide_m4F44530197AA791474C6EB6DA5AE8769537F3B0C (void);
// 0x000000A8 System.Void SimpleFileBrowser.FileBrowserContextMenu::RefreshSkin(SimpleFileBrowser.UISkin)
extern void FileBrowserContextMenu_RefreshSkin_mA4DA4B81E3CD5F07DAC9C1C03812E99F8517E58F (void);
// 0x000000A9 System.Void SimpleFileBrowser.FileBrowserContextMenu::OnSelectAllButtonClicked()
extern void FileBrowserContextMenu_OnSelectAllButtonClicked_m69A4007CE3F55DB2B417588BE76C36D91BE89E14 (void);
// 0x000000AA System.Void SimpleFileBrowser.FileBrowserContextMenu::OnDeselectAllButtonClicked()
extern void FileBrowserContextMenu_OnDeselectAllButtonClicked_mB3184AE7626029BAFD1FF6421DD1C017931B8599 (void);
// 0x000000AB System.Void SimpleFileBrowser.FileBrowserContextMenu::OnCreateFolderButtonClicked()
extern void FileBrowserContextMenu_OnCreateFolderButtonClicked_m02009A1FD862FC3F0F9DBCC20EDFEA1BD11AF5C9 (void);
// 0x000000AC System.Void SimpleFileBrowser.FileBrowserContextMenu::OnDeleteButtonClicked()
extern void FileBrowserContextMenu_OnDeleteButtonClicked_m8658E87083F4D963BC3B705E60CD2E11EC211A99 (void);
// 0x000000AD System.Void SimpleFileBrowser.FileBrowserContextMenu::OnRenameButtonClicked()
extern void FileBrowserContextMenu_OnRenameButtonClicked_m9001A3AC6E30F35E99624014C923C63EC173EB19 (void);
// 0x000000AE System.Void SimpleFileBrowser.FileBrowserContextMenu::.ctor()
extern void FileBrowserContextMenu__ctor_mD48F391D8AE027F325EB80BDB8033012AAB83C04 (void);
// 0x000000AF System.Void SimpleFileBrowser.FileBrowserCursorHandler::UnityEngine.EventSystems.IPointerEnterHandler.OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserCursorHandler_UnityEngine_EventSystems_IPointerEnterHandler_OnPointerEnter_mD3E85EB94541AC1352CF0267D031B058FA9B3731 (void);
// 0x000000B0 System.Void SimpleFileBrowser.FileBrowserCursorHandler::UnityEngine.EventSystems.IPointerExitHandler.OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserCursorHandler_UnityEngine_EventSystems_IPointerExitHandler_OnPointerExit_mC51857736F09A3F196DBB341D877B977B6F300E5 (void);
// 0x000000B1 System.Void SimpleFileBrowser.FileBrowserCursorHandler::UnityEngine.EventSystems.IBeginDragHandler.OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserCursorHandler_UnityEngine_EventSystems_IBeginDragHandler_OnBeginDrag_m3E295A9B3006C9B59750719B81C8F58D517D328C (void);
// 0x000000B2 System.Void SimpleFileBrowser.FileBrowserCursorHandler::UnityEngine.EventSystems.IEndDragHandler.OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserCursorHandler_UnityEngine_EventSystems_IEndDragHandler_OnEndDrag_m2871B399DF1FB6D0C079F8A2E6621D917FF3CA14 (void);
// 0x000000B3 System.Void SimpleFileBrowser.FileBrowserCursorHandler::ShowDefaultCursor()
extern void FileBrowserCursorHandler_ShowDefaultCursor_mC705BE56E9F59A8124D332B831AF6F5D8AC1FFBE (void);
// 0x000000B4 System.Void SimpleFileBrowser.FileBrowserCursorHandler::ShowResizeCursor()
extern void FileBrowserCursorHandler_ShowResizeCursor_m4FD081FBE47A419227A5032A8D065B60ECB6EB32 (void);
// 0x000000B5 System.Void SimpleFileBrowser.FileBrowserCursorHandler::.ctor()
extern void FileBrowserCursorHandler__ctor_mE7AF5F81DBF3EB93910E74617D29B4C5C62A728A (void);
// 0x000000B6 System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::Show(SimpleFileBrowser.FileBrowser,System.Collections.Generic.List`1<SimpleFileBrowser.FileSystemEntry>,System.Collections.Generic.List`1<System.Int32>,SimpleFileBrowser.FileBrowserDeleteConfirmationPanel/OnDeletionConfirmed)
extern void FileBrowserDeleteConfirmationPanel_Show_m67A714F441AE140570CF68216DD4BB607EAEE0F4 (void);
// 0x000000B7 System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::OnCanvasDimensionsChanged(UnityEngine.Vector2)
extern void FileBrowserDeleteConfirmationPanel_OnCanvasDimensionsChanged_mEBED06B7C2B79D72BB46D075BA9623E236568C78 (void);
// 0x000000B8 System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::RefreshSkin(SimpleFileBrowser.UISkin)
extern void FileBrowserDeleteConfirmationPanel_RefreshSkin_mA988F9B82D73A78E3357655388A014D7F1DC8218 (void);
// 0x000000B9 System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::YesButtonClicked()
extern void FileBrowserDeleteConfirmationPanel_YesButtonClicked_mD4E097A0309A4F375F29CA4A84A04B17C8D9C06C (void);
// 0x000000BA System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::NoButtonClicked()
extern void FileBrowserDeleteConfirmationPanel_NoButtonClicked_m06E216DACB4F48FF98117633CE5046C2A53D84BD (void);
// 0x000000BB System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel::.ctor()
extern void FileBrowserDeleteConfirmationPanel__ctor_m7FD9E661764ED42230B0FE31232F0F23AB464B81 (void);
// 0x000000BC System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel/OnDeletionConfirmed::.ctor(System.Object,System.IntPtr)
extern void OnDeletionConfirmed__ctor_m6D56888CB1E3F037AD52E31DCBA82ACE8A94BADF (void);
// 0x000000BD System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel/OnDeletionConfirmed::Invoke()
extern void OnDeletionConfirmed_Invoke_mA3D4CA80D75F726B8B66054C3BD58CF213B4338B (void);
// 0x000000BE System.IAsyncResult SimpleFileBrowser.FileBrowserDeleteConfirmationPanel/OnDeletionConfirmed::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnDeletionConfirmed_BeginInvoke_mC621E4FD8F54E439AB1F41CEDAE33C0E2DE3345C (void);
// 0x000000BF System.Void SimpleFileBrowser.FileBrowserDeleteConfirmationPanel/OnDeletionConfirmed::EndInvoke(System.IAsyncResult)
extern void OnDeletionConfirmed_EndInvoke_m9F6DB6C94C05198F5F7F746FDF610E37BF40B0B5 (void);
// 0x000000C0 System.Boolean SimpleFileBrowser.FileSystemEntry::get_IsDirectory()
extern void FileSystemEntry_get_IsDirectory_m5B6EAD6DAC01DC97FA18BF8C82FB57CD009F9E53 (void);
// 0x000000C1 System.Void SimpleFileBrowser.FileSystemEntry::.ctor(System.String,System.String,System.String,System.Boolean)
extern void FileSystemEntry__ctor_mFB6D7113F6EC140A0863D55B690ACADC5A22EBD1 (void);
// 0x000000C2 System.Void SimpleFileBrowser.FileSystemEntry::.ctor(System.IO.FileSystemInfo,System.String)
extern void FileSystemEntry__ctor_mF3DAAE9ECE05D268F76C8A7A8E9B23083B699C22 (void);
// 0x000000C3 System.Boolean SimpleFileBrowser.FileBrowserHelpers::FileExists(System.String)
extern void FileBrowserHelpers_FileExists_mD73A1D794A8CCB271F1BA746A082354E338F57FD (void);
// 0x000000C4 System.Boolean SimpleFileBrowser.FileBrowserHelpers::DirectoryExists(System.String)
extern void FileBrowserHelpers_DirectoryExists_m34D0C75C33EABE4964762FC0C9B8B8446FF6C335 (void);
// 0x000000C5 System.Boolean SimpleFileBrowser.FileBrowserHelpers::IsDirectory(System.String)
extern void FileBrowserHelpers_IsDirectory_m2E0623A10A6163784CF86F92DF6345C7AB2C8650 (void);
// 0x000000C6 System.String SimpleFileBrowser.FileBrowserHelpers::GetDirectoryName(System.String)
extern void FileBrowserHelpers_GetDirectoryName_m6342C65603EF0B25E2C2382773027263E5AB1817 (void);
// 0x000000C7 SimpleFileBrowser.FileSystemEntry[] SimpleFileBrowser.FileBrowserHelpers::GetEntriesInDirectory(System.String,System.Boolean)
extern void FileBrowserHelpers_GetEntriesInDirectory_m4E5FA288844CAA15C783E32EB39CD6C062B37AC3 (void);
// 0x000000C8 System.String SimpleFileBrowser.FileBrowserHelpers::CreateFileInDirectory(System.String,System.String)
extern void FileBrowserHelpers_CreateFileInDirectory_m494CB83E0BB762403696BF033B728AD1D43C6477 (void);
// 0x000000C9 System.String SimpleFileBrowser.FileBrowserHelpers::CreateFolderInDirectory(System.String,System.String)
extern void FileBrowserHelpers_CreateFolderInDirectory_mAEE39C8930FB9F6A6972D87CE827CD8EA6C7B9A7 (void);
// 0x000000CA System.Void SimpleFileBrowser.FileBrowserHelpers::WriteBytesToFile(System.String,System.Byte[])
extern void FileBrowserHelpers_WriteBytesToFile_m8044FC5F818D349E6B4AB56D2C7C71E76B1B9967 (void);
// 0x000000CB System.Void SimpleFileBrowser.FileBrowserHelpers::WriteTextToFile(System.String,System.String)
extern void FileBrowserHelpers_WriteTextToFile_m296944773E9E3939C4985D8754D4C13E5599A706 (void);
// 0x000000CC System.Void SimpleFileBrowser.FileBrowserHelpers::AppendBytesToFile(System.String,System.Byte[])
extern void FileBrowserHelpers_AppendBytesToFile_m156371AE42B0661E70DEDD24812F02A7096C9F95 (void);
// 0x000000CD System.Void SimpleFileBrowser.FileBrowserHelpers::AppendTextToFile(System.String,System.String)
extern void FileBrowserHelpers_AppendTextToFile_m2C9CCC9F3D53F3B3FEDD5B899852239580ED2754 (void);
// 0x000000CE System.Void SimpleFileBrowser.FileBrowserHelpers::AppendFileToFile(System.String,System.String)
extern void FileBrowserHelpers_AppendFileToFile_m22CEECB47EF2E23DAD9A2B472392AAAD66BF59F9 (void);
// 0x000000CF System.Byte[] SimpleFileBrowser.FileBrowserHelpers::ReadBytesFromFile(System.String)
extern void FileBrowserHelpers_ReadBytesFromFile_m6A2C4FF9E1A0805920FD1A10E41FE822DE6B2C21 (void);
// 0x000000D0 System.String SimpleFileBrowser.FileBrowserHelpers::ReadTextFromFile(System.String)
extern void FileBrowserHelpers_ReadTextFromFile_m29394418D7A8B9A7B66004DF8AC2E2C37B68C566 (void);
// 0x000000D1 System.Void SimpleFileBrowser.FileBrowserHelpers::CopyFile(System.String,System.String)
extern void FileBrowserHelpers_CopyFile_mC01E97041A6955509A71728024D34BD35CD69C3B (void);
// 0x000000D2 System.Void SimpleFileBrowser.FileBrowserHelpers::CopyDirectory(System.String,System.String)
extern void FileBrowserHelpers_CopyDirectory_m99947742CE8BC2D9E71E1F419F36C87C788C4B4D (void);
// 0x000000D3 System.Void SimpleFileBrowser.FileBrowserHelpers::CopyDirectoryRecursively(System.IO.DirectoryInfo,System.String)
extern void FileBrowserHelpers_CopyDirectoryRecursively_mC86E85947B557233487229377CC3F6BB6C3F00F0 (void);
// 0x000000D4 System.Void SimpleFileBrowser.FileBrowserHelpers::MoveFile(System.String,System.String)
extern void FileBrowserHelpers_MoveFile_m6D1090980ED4B6A9C2C7510474E73495794FC978 (void);
// 0x000000D5 System.Void SimpleFileBrowser.FileBrowserHelpers::MoveDirectory(System.String,System.String)
extern void FileBrowserHelpers_MoveDirectory_m97F8E459D43028605A977F6C63E2DD1AA0450F66 (void);
// 0x000000D6 System.String SimpleFileBrowser.FileBrowserHelpers::RenameFile(System.String,System.String)
extern void FileBrowserHelpers_RenameFile_m28EC14810CA6B6A17C271ABC2AC2BFA21DDE67D9 (void);
// 0x000000D7 System.String SimpleFileBrowser.FileBrowserHelpers::RenameDirectory(System.String,System.String)
extern void FileBrowserHelpers_RenameDirectory_m006A6B9AAECF8288AF9D689AC23C54F6E6518DEB (void);
// 0x000000D8 System.Void SimpleFileBrowser.FileBrowserHelpers::DeleteFile(System.String)
extern void FileBrowserHelpers_DeleteFile_m9FE24F1EEA853DE05BE18E9DA85D014C73360AD8 (void);
// 0x000000D9 System.Void SimpleFileBrowser.FileBrowserHelpers::DeleteDirectory(System.String)
extern void FileBrowserHelpers_DeleteDirectory_m2812DF984D0E379CD2185D077FF519AB5FA717EB (void);
// 0x000000DA System.String SimpleFileBrowser.FileBrowserHelpers::GetFilename(System.String)
extern void FileBrowserHelpers_GetFilename_mA8E038218E294D76E4DAFD17571427175F8E24BE (void);
// 0x000000DB System.Int64 SimpleFileBrowser.FileBrowserHelpers::GetFilesize(System.String)
extern void FileBrowserHelpers_GetFilesize_mD732CD14CE50897F645BC05449BB668FFEB0D8A1 (void);
// 0x000000DC System.DateTime SimpleFileBrowser.FileBrowserHelpers::GetLastModifiedDate(System.String)
extern void FileBrowserHelpers_GetLastModifiedDate_m36E0A235A22F2472CA30E88F848A44EB50C0B540 (void);
// 0x000000DD UnityEngine.UI.Image SimpleFileBrowser.FileBrowserItem::get_Icon()
extern void FileBrowserItem_get_Icon_mA236A217FDF67ABF9C1650452ADD466428416208 (void);
// 0x000000DE UnityEngine.RectTransform SimpleFileBrowser.FileBrowserItem::get_TransformComponent()
extern void FileBrowserItem_get_TransformComponent_m54A7F99BE536DE843DFD8C2308122A7BA77CFFD9 (void);
// 0x000000DF System.String SimpleFileBrowser.FileBrowserItem::get_Name()
extern void FileBrowserItem_get_Name_mB4AB269ACE59C310314C422DE06883842C6B1A15 (void);
// 0x000000E0 System.Boolean SimpleFileBrowser.FileBrowserItem::get_IsDirectory()
extern void FileBrowserItem_get_IsDirectory_mE2B9AE6ADA43DF5A8408EE693F2FDDD88D216D0F (void);
// 0x000000E1 System.Void SimpleFileBrowser.FileBrowserItem::set_IsDirectory(System.Boolean)
extern void FileBrowserItem_set_IsDirectory_mA1AD3AFE7B0E46210FE1289FE2D291046DEF66F7 (void);
// 0x000000E2 System.Void SimpleFileBrowser.FileBrowserItem::SetFileBrowser(SimpleFileBrowser.FileBrowser,SimpleFileBrowser.UISkin)
extern void FileBrowserItem_SetFileBrowser_m113A09B2B5105907F7A0565AABD1208E96E1DD35 (void);
// 0x000000E3 System.Void SimpleFileBrowser.FileBrowserItem::SetFile(UnityEngine.Sprite,System.String,System.Boolean)
extern void FileBrowserItem_SetFile_m01704B1D968B4F8DCFD399DC0D9FE8DC07A36DE5 (void);
// 0x000000E4 System.Void SimpleFileBrowser.FileBrowserItem::Update()
extern void FileBrowserItem_Update_m57C36405867A0ECD4291F78C6C0068A00C66C1E0 (void);
// 0x000000E5 System.Void SimpleFileBrowser.FileBrowserItem::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserItem_OnPointerClick_mA105524535DA9304C54057B9FBEFC4DC607E4FEF (void);
// 0x000000E6 System.Void SimpleFileBrowser.FileBrowserItem::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserItem_OnPointerDown_mE37EAEE7FDF98BB1F7A9B349D3B3072A6132EC63 (void);
// 0x000000E7 System.Void SimpleFileBrowser.FileBrowserItem::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserItem_OnPointerUp_mD4DAF0E75338718E388E09FB38D79C3A6B3A9616 (void);
// 0x000000E8 System.Void SimpleFileBrowser.FileBrowserItem::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserItem_OnPointerEnter_m6E522B31AA73A624A7E2C1E0C072A2B9B37B5810 (void);
// 0x000000E9 System.Void SimpleFileBrowser.FileBrowserItem::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void FileBrowserItem_OnPointerExit_m53AA3FB9005B0A92FA62C66BA1EED14DE61E1982 (void);
// 0x000000EA System.Void SimpleFileBrowser.FileBrowserItem::SetSelected(System.Boolean)
extern void FileBrowserItem_SetSelected_mD98A715E8753CAAC93243CE7E865BB3E981FE3BB (void);
// 0x000000EB System.Void SimpleFileBrowser.FileBrowserItem::SetHidden(System.Boolean)
extern void FileBrowserItem_SetHidden_mC9BA839FA2A4429730135DC29989E8B441E76525 (void);
// 0x000000EC System.Void SimpleFileBrowser.FileBrowserItem::OnSkinRefreshed(SimpleFileBrowser.UISkin,System.Boolean)
extern void FileBrowserItem_OnSkinRefreshed_m9ACDA69EDA9861AC47C8A27F02984C03291C1051 (void);
// 0x000000ED System.Void SimpleFileBrowser.FileBrowserItem::.ctor()
extern void FileBrowserItem__ctor_mFD76D8703595FF18AE3C0C30455FD2E877BC5DB8 (void);
// 0x000000EE System.Void SimpleFileBrowser.FileBrowserMovement::Initialize(SimpleFileBrowser.FileBrowser)
extern void FileBrowserMovement_Initialize_mE6E2DCBA906CED5E93919B74BACC873B18DC8C5A (void);
// 0x000000EF System.Void SimpleFileBrowser.FileBrowserMovement::OnDragStarted(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnDragStarted_mB878CDC08ABE141804FA0E6DC015D25C2F4E8C02 (void);
// 0x000000F0 System.Void SimpleFileBrowser.FileBrowserMovement::OnDrag(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnDrag_m3CA70167EDA0F00781090D5698F1B5B6A9D95F1B (void);
// 0x000000F1 System.Void SimpleFileBrowser.FileBrowserMovement::OnEndDrag(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnEndDrag_m7CD70CC03FEF03871A31AECFE52D69AD85D4DB9B (void);
// 0x000000F2 System.Void SimpleFileBrowser.FileBrowserMovement::OnResizeStarted(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnResizeStarted_m7C591C6E5CDF08E378B0404CC6E491B93D4F6DEE (void);
// 0x000000F3 System.Void SimpleFileBrowser.FileBrowserMovement::OnResize(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnResize_m2F86FCF2FFFE5B82FD8F8E33400488B3AFC63165 (void);
// 0x000000F4 System.Void SimpleFileBrowser.FileBrowserMovement::OnEndResize(UnityEngine.EventSystems.BaseEventData)
extern void FileBrowserMovement_OnEndResize_m8425FEDD990DD74A6574095F42D184CD6A5DD4CE (void);
// 0x000000F5 System.Void SimpleFileBrowser.FileBrowserMovement::.ctor()
extern void FileBrowserMovement__ctor_mD8568EF6C0AA08152B980E6D05BD4879030B4F8E (void);
// 0x000000F6 System.String SimpleFileBrowser.FileBrowserQuickLink::get_TargetPath()
extern void FileBrowserQuickLink_get_TargetPath_mFF4A822E3C9FFDE3F531E1376E6B1797D64AD756 (void);
// 0x000000F7 System.Void SimpleFileBrowser.FileBrowserQuickLink::SetQuickLink(UnityEngine.Sprite,System.String,System.String)
extern void FileBrowserQuickLink_SetQuickLink_mCA309400C8524097232B24883F14BAFE92D0C900 (void);
// 0x000000F8 System.Void SimpleFileBrowser.FileBrowserQuickLink::.ctor()
extern void FileBrowserQuickLink__ctor_m5BDF5B0833F6CC163E18BCCFDF01CCE9DCADC3D1 (void);
// 0x000000F9 UnityEngine.UI.InputField SimpleFileBrowser.FileBrowserRenamedItem::get_InputField()
extern void FileBrowserRenamedItem_get_InputField_m91346D55198B298E74E3BCF1E0F0217F89BDDDD4 (void);
// 0x000000FA UnityEngine.RectTransform SimpleFileBrowser.FileBrowserRenamedItem::get_TransformComponent()
extern void FileBrowserRenamedItem_get_TransformComponent_m5028BCA8A4B50F5F12E62676D6606F29DD8D54CD (void);
// 0x000000FB System.Void SimpleFileBrowser.FileBrowserRenamedItem::Show(System.String,UnityEngine.Color,UnityEngine.Sprite,SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted)
extern void FileBrowserRenamedItem_Show_m55D9787654356551E45B68F837AF154EB31613F0 (void);
// 0x000000FC System.Void SimpleFileBrowser.FileBrowserRenamedItem::OnInputFieldEndEdit(System.String)
extern void FileBrowserRenamedItem_OnInputFieldEndEdit_m5A25798DA5AB1238A465A6F2FB29C87527402452 (void);
// 0x000000FD System.Void SimpleFileBrowser.FileBrowserRenamedItem::.ctor()
extern void FileBrowserRenamedItem__ctor_m8638B851D48F656DD2878E36B40CED94587E9A92 (void);
// 0x000000FE System.Void SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::.ctor(System.Object,System.IntPtr)
extern void OnRenameCompleted__ctor_m48E13B91ED45B15352E140198FC3415D9A888858 (void);
// 0x000000FF System.Void SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::Invoke(System.String)
extern void OnRenameCompleted_Invoke_m383F016C41D7C2B8E78DBA3BA5130DE979344E21 (void);
// 0x00000100 System.IAsyncResult SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void OnRenameCompleted_BeginInvoke_mEA6D5D7CE1A2D85340E70E28964064BFDD3247E2 (void);
// 0x00000101 System.Void SimpleFileBrowser.FileBrowserRenamedItem/OnRenameCompleted::EndInvoke(System.IAsyncResult)
extern void OnRenameCompleted_EndInvoke_m296469B8DE1FD1150FA9EDC0F28A386571EA0F95 (void);
// 0x00000102 System.Void SimpleFileBrowser.NonDrawingGraphic::SetMaterialDirty()
extern void NonDrawingGraphic_SetMaterialDirty_m64684F184E0307DE955559C0E57F748B41B879A4 (void);
// 0x00000103 System.Void SimpleFileBrowser.NonDrawingGraphic::SetVerticesDirty()
extern void NonDrawingGraphic_SetVerticesDirty_m8B4982B91710D4046B2751FFD62A6B193A100AB6 (void);
// 0x00000104 System.Void SimpleFileBrowser.NonDrawingGraphic::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void NonDrawingGraphic_OnPopulateMesh_mE2F59B5470ACE70E2934C5A873CFE863F06E454C (void);
// 0x00000105 System.Void SimpleFileBrowser.NonDrawingGraphic::.ctor()
extern void NonDrawingGraphic__ctor_m561CB90871C92DBC8E0839BA9C92D5929551709F (void);
// 0x00000106 System.Void SimpleFileBrowser.OnItemClickedHandler::.ctor(System.Object,System.IntPtr)
extern void OnItemClickedHandler__ctor_mF80210604B9059FD235A2C0339A14F815DF69314 (void);
// 0x00000107 System.Void SimpleFileBrowser.OnItemClickedHandler::Invoke(SimpleFileBrowser.ListItem)
extern void OnItemClickedHandler_Invoke_m35BB1924A7FD2EDADA9D5BF729B37C3E1D7694EA (void);
// 0x00000108 System.IAsyncResult SimpleFileBrowser.OnItemClickedHandler::BeginInvoke(SimpleFileBrowser.ListItem,System.AsyncCallback,System.Object)
extern void OnItemClickedHandler_BeginInvoke_mEDA83A6B63004CDECEEF29096BBF7F73BC00843B (void);
// 0x00000109 System.Void SimpleFileBrowser.OnItemClickedHandler::EndInvoke(System.IAsyncResult)
extern void OnItemClickedHandler_EndInvoke_mB9E3F49BD94DCB436D005D25DC07B28A5DD44AF1 (void);
// 0x0000010A SimpleFileBrowser.OnItemClickedHandler SimpleFileBrowser.IListViewAdapter::get_OnItemClicked()
// 0x0000010B System.Void SimpleFileBrowser.IListViewAdapter::set_OnItemClicked(SimpleFileBrowser.OnItemClickedHandler)
// 0x0000010C System.Int32 SimpleFileBrowser.IListViewAdapter::get_Count()
// 0x0000010D System.Single SimpleFileBrowser.IListViewAdapter::get_ItemHeight()
// 0x0000010E SimpleFileBrowser.ListItem SimpleFileBrowser.IListViewAdapter::CreateItem()
// 0x0000010F System.Void SimpleFileBrowser.IListViewAdapter::SetItemContent(SimpleFileBrowser.ListItem)
// 0x00000110 System.Object SimpleFileBrowser.ListItem::get_Tag()
extern void ListItem_get_Tag_m54D91B8F78C5BF1987B21B392983057ECED6D383 (void);
// 0x00000111 System.Void SimpleFileBrowser.ListItem::set_Tag(System.Object)
extern void ListItem_set_Tag_mF48352498EE087564986E5BF9E9CDACA78C342EA (void);
// 0x00000112 System.Int32 SimpleFileBrowser.ListItem::get_Position()
extern void ListItem_get_Position_m49214519D4BBE5B0F33D6AAB4E3B8F49B580DEFD (void);
// 0x00000113 System.Void SimpleFileBrowser.ListItem::set_Position(System.Int32)
extern void ListItem_set_Position_m8605AB6B00F33563B51063DAEE72D65110B2CBAD (void);
// 0x00000114 System.Void SimpleFileBrowser.ListItem::SetAdapter(SimpleFileBrowser.IListViewAdapter)
extern void ListItem_SetAdapter_m72EBAFC39C01195C647B1EC6E0A57D7D0B378D1E (void);
// 0x00000115 System.Void SimpleFileBrowser.ListItem::OnClick()
extern void ListItem_OnClick_m251412E71A6074C6C7BA2101009FC298987A481A (void);
// 0x00000116 System.Void SimpleFileBrowser.ListItem::.ctor()
extern void ListItem__ctor_m0F2F59D6C18F974CC520A3E73153EA9C25AE2353 (void);
// 0x00000117 System.Void SimpleFileBrowser.RecycledListView::Start()
extern void RecycledListView_Start_m624BF5D74E7F55A5344C9ED80773F6CC601939DC (void);
// 0x00000118 System.Void SimpleFileBrowser.RecycledListView::SetAdapter(SimpleFileBrowser.IListViewAdapter)
extern void RecycledListView_SetAdapter_mF900098FEB7DCE2041BE234FD593A9DDD7E20699 (void);
// 0x00000119 System.Void SimpleFileBrowser.RecycledListView::OnSkinRefreshed()
extern void RecycledListView_OnSkinRefreshed_mA211E24C82988639442BFFB7077CCC5073C7B3EC (void);
// 0x0000011A System.Void SimpleFileBrowser.RecycledListView::UpdateList()
extern void RecycledListView_UpdateList_mDB5B2A69EA3A9DD770D372547E3F183554B3EE4F (void);
// 0x0000011B System.Void SimpleFileBrowser.RecycledListView::OnViewportDimensionsChanged()
extern void RecycledListView_OnViewportDimensionsChanged_mB6C2DC3A7A045804CA21594CADB070B00EA8C04B (void);
// 0x0000011C System.Void SimpleFileBrowser.RecycledListView::UpdateItemsInTheList(System.Boolean)
extern void RecycledListView_UpdateItemsInTheList_m55C6ABE3F27C06D0AC7483627ED8D51FBAF83AF4 (void);
// 0x0000011D System.Void SimpleFileBrowser.RecycledListView::CreateItemsBetweenIndices(System.Int32,System.Int32)
extern void RecycledListView_CreateItemsBetweenIndices_m429BB2B660C88407528C6F8D3EDE86B7AC688E85 (void);
// 0x0000011E System.Void SimpleFileBrowser.RecycledListView::CreateItemAtIndex(System.Int32)
extern void RecycledListView_CreateItemAtIndex_m0A47C8737153B21BFD8616E3DC9FC3E0FA678404 (void);
// 0x0000011F System.Void SimpleFileBrowser.RecycledListView::DestroyItemsBetweenIndices(System.Int32,System.Int32)
extern void RecycledListView_DestroyItemsBetweenIndices_m595074E0ACC185C718E71BD88AFF7EB5C7DE6201 (void);
// 0x00000120 System.Void SimpleFileBrowser.RecycledListView::UpdateItemContentsBetweenIndices(System.Int32,System.Int32)
extern void RecycledListView_UpdateItemContentsBetweenIndices_mE71583AFA6393C6B6A2358F76927FB069438B7C9 (void);
// 0x00000121 System.Void SimpleFileBrowser.RecycledListView::.ctor()
extern void RecycledListView__ctor_m883D98DDCC2B6870B849DA84AB7245DE5CC3314F (void);
// 0x00000122 System.Void SimpleFileBrowser.RecycledListView::<Start>b__10_0(UnityEngine.Vector2)
extern void RecycledListView_U3CStartU3Eb__10_0_mB8CB8298DCD1DFA739C0DD5C6394175D0C9D80EA (void);
// 0x00000123 System.Int32 SimpleFileBrowser.UISkin::get_Version()
extern void UISkin_get_Version_mDAF7A3DD2F270AF0E34DB56BE95481EFEBE3D150 (void);
// 0x00000124 System.Void SimpleFileBrowser.UISkin::Invalidate()
extern void UISkin_Invalidate_mBDEEA141046DC463BFFCE1E4E3F3EEEFF0DE066F (void);
// 0x00000125 UnityEngine.Font SimpleFileBrowser.UISkin::get_Font()
extern void UISkin_get_Font_mF4B20BA9562F4CDEFE7F72CBA8A2EDC481DE800B (void);
// 0x00000126 System.Void SimpleFileBrowser.UISkin::set_Font(UnityEngine.Font)
extern void UISkin_set_Font_mDC0C97F15A599D1540DB2767935F94BC7689B1A7 (void);
// 0x00000127 System.Int32 SimpleFileBrowser.UISkin::get_FontSize()
extern void UISkin_get_FontSize_mC5BD7D44ABDB28F2D804938F5CD1E6FE82970700 (void);
// 0x00000128 System.Void SimpleFileBrowser.UISkin::set_FontSize(System.Int32)
extern void UISkin_set_FontSize_mF2097F11D9B90DA7031380B2AADD228B48017C3F (void);
// 0x00000129 UnityEngine.Color SimpleFileBrowser.UISkin::get_WindowColor()
extern void UISkin_get_WindowColor_m587EB65FACDE434DBCC5E2961D517FB0071F2B9B (void);
// 0x0000012A System.Void SimpleFileBrowser.UISkin::set_WindowColor(UnityEngine.Color)
extern void UISkin_set_WindowColor_m87B53B161651DD26C478F3F33DF5F4625DCC8AE3 (void);
// 0x0000012B UnityEngine.Color SimpleFileBrowser.UISkin::get_FilesListColor()
extern void UISkin_get_FilesListColor_m7DAED2AD9BE8CDBA0C38BF2A153E8117B215A8DC (void);
// 0x0000012C System.Void SimpleFileBrowser.UISkin::set_FilesListColor(UnityEngine.Color)
extern void UISkin_set_FilesListColor_mCA788F63367DE069F8FD1664C7BEC1CD5872C00E (void);
// 0x0000012D UnityEngine.Color SimpleFileBrowser.UISkin::get_FilesVerticalSeparatorColor()
extern void UISkin_get_FilesVerticalSeparatorColor_mC7398F4BC0B44798A0049B102FE5CF2ECA119A8B (void);
// 0x0000012E System.Void SimpleFileBrowser.UISkin::set_FilesVerticalSeparatorColor(UnityEngine.Color)
extern void UISkin_set_FilesVerticalSeparatorColor_m433BE8490F69502A0BC7957B6304A7E6AC2B903F (void);
// 0x0000012F UnityEngine.Color SimpleFileBrowser.UISkin::get_TitleBackgroundColor()
extern void UISkin_get_TitleBackgroundColor_mE9842CD030A5C2711F49068385F3CF209162B175 (void);
// 0x00000130 System.Void SimpleFileBrowser.UISkin::set_TitleBackgroundColor(UnityEngine.Color)
extern void UISkin_set_TitleBackgroundColor_m7D6171508315316F2854F03A4092CB68E7AF6069 (void);
// 0x00000131 UnityEngine.Color SimpleFileBrowser.UISkin::get_TitleTextColor()
extern void UISkin_get_TitleTextColor_m0EFEAD3F0F07BC93BB02F3ED98399FF5DA701864 (void);
// 0x00000132 System.Void SimpleFileBrowser.UISkin::set_TitleTextColor(UnityEngine.Color)
extern void UISkin_set_TitleTextColor_m95C293827628CE2ABE320F18FBF3AB894B870F11 (void);
// 0x00000133 UnityEngine.Color SimpleFileBrowser.UISkin::get_WindowResizeGizmoColor()
extern void UISkin_get_WindowResizeGizmoColor_m05BE6C8ED3632A4A18C82E91C066A7B016AEC568 (void);
// 0x00000134 System.Void SimpleFileBrowser.UISkin::set_WindowResizeGizmoColor(UnityEngine.Color)
extern void UISkin_set_WindowResizeGizmoColor_mBBBD0B6572664BD4970DB330E2EB39D5D1569BA2 (void);
// 0x00000135 UnityEngine.Color SimpleFileBrowser.UISkin::get_HeaderButtonsColor()
extern void UISkin_get_HeaderButtonsColor_m11E813AEA79CF48086C20C8049DA7CB8144A970C (void);
// 0x00000136 System.Void SimpleFileBrowser.UISkin::set_HeaderButtonsColor(UnityEngine.Color)
extern void UISkin_set_HeaderButtonsColor_mE7B34CFF77ACE5708979708D5601A43FB4FEE1B9 (void);
// 0x00000137 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_WindowResizeGizmo()
extern void UISkin_get_WindowResizeGizmo_mDBEB8CE46B80527E0F33AE2173A305BFF3F9E18D (void);
// 0x00000138 System.Void SimpleFileBrowser.UISkin::set_WindowResizeGizmo(UnityEngine.Sprite)
extern void UISkin_set_WindowResizeGizmo_m1A4EC118F3F62BF5C11682E0652004AD2F2823B9 (void);
// 0x00000139 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_HeaderBackButton()
extern void UISkin_get_HeaderBackButton_mBE8EA5E2A83099BE0D0200FA395E08F7DEB22EF7 (void);
// 0x0000013A System.Void SimpleFileBrowser.UISkin::set_HeaderBackButton(UnityEngine.Sprite)
extern void UISkin_set_HeaderBackButton_m2105844942D1764173DE589E5B57DE198948A29D (void);
// 0x0000013B UnityEngine.Sprite SimpleFileBrowser.UISkin::get_HeaderForwardButton()
extern void UISkin_get_HeaderForwardButton_m8B3822B153FBE2F369C08E0C3CEFDC87EAB90826 (void);
// 0x0000013C System.Void SimpleFileBrowser.UISkin::set_HeaderForwardButton(UnityEngine.Sprite)
extern void UISkin_set_HeaderForwardButton_m1A9B67C4EF1E2EF6EE3868086978314F8D475AAD (void);
// 0x0000013D UnityEngine.Sprite SimpleFileBrowser.UISkin::get_HeaderUpButton()
extern void UISkin_get_HeaderUpButton_m8462D0BB1A8D5AD173E463D6C200BBE5799268ED (void);
// 0x0000013E System.Void SimpleFileBrowser.UISkin::set_HeaderUpButton(UnityEngine.Sprite)
extern void UISkin_set_HeaderUpButton_m6F2590A5A6177E98192F4F8963802C1BCBFBBA3A (void);
// 0x0000013F UnityEngine.Sprite SimpleFileBrowser.UISkin::get_HeaderContextMenuButton()
extern void UISkin_get_HeaderContextMenuButton_m2C886896865E1BD03E061FE941C52410A5A3ADC8 (void);
// 0x00000140 System.Void SimpleFileBrowser.UISkin::set_HeaderContextMenuButton(UnityEngine.Sprite)
extern void UISkin_set_HeaderContextMenuButton_m1BCC96265FBBBA16D969AA3F42110247CB6FEEAB (void);
// 0x00000141 UnityEngine.Color SimpleFileBrowser.UISkin::get_InputFieldNormalBackgroundColor()
extern void UISkin_get_InputFieldNormalBackgroundColor_m3F438650067F2332CD8726F3EC1F902D374AFFDD (void);
// 0x00000142 System.Void SimpleFileBrowser.UISkin::set_InputFieldNormalBackgroundColor(UnityEngine.Color)
extern void UISkin_set_InputFieldNormalBackgroundColor_mD9DE1BA2802C58B85F3A66EA9858BA0ABBE0F30F (void);
// 0x00000143 UnityEngine.Color SimpleFileBrowser.UISkin::get_InputFieldInvalidBackgroundColor()
extern void UISkin_get_InputFieldInvalidBackgroundColor_mDB2E76A76311F129618DDFBA57C416EBE0C1CCF1 (void);
// 0x00000144 System.Void SimpleFileBrowser.UISkin::set_InputFieldInvalidBackgroundColor(UnityEngine.Color)
extern void UISkin_set_InputFieldInvalidBackgroundColor_m5C4AA5F815E20730A067A2BE6ECE0B1000E4C0F2 (void);
// 0x00000145 UnityEngine.Color SimpleFileBrowser.UISkin::get_InputFieldTextColor()
extern void UISkin_get_InputFieldTextColor_m2C432E5F4DD75770C2E537C8996BBC5A67691883 (void);
// 0x00000146 System.Void SimpleFileBrowser.UISkin::set_InputFieldTextColor(UnityEngine.Color)
extern void UISkin_set_InputFieldTextColor_mED924C2919107F7F6735B3C860DE20ECEBF5820E (void);
// 0x00000147 UnityEngine.Color SimpleFileBrowser.UISkin::get_InputFieldPlaceholderTextColor()
extern void UISkin_get_InputFieldPlaceholderTextColor_mA9480AE69A17149110E4B23FB68ECA33485C6207 (void);
// 0x00000148 System.Void SimpleFileBrowser.UISkin::set_InputFieldPlaceholderTextColor(UnityEngine.Color)
extern void UISkin_set_InputFieldPlaceholderTextColor_mA4F622FF2AB41D72C630DEBE09EBEF3969E747BA (void);
// 0x00000149 UnityEngine.Color SimpleFileBrowser.UISkin::get_InputFieldSelectedTextColor()
extern void UISkin_get_InputFieldSelectedTextColor_mD5FB460350A6BAB9D2B14DCF1679339134A97EBD (void);
// 0x0000014A System.Void SimpleFileBrowser.UISkin::set_InputFieldSelectedTextColor(UnityEngine.Color)
extern void UISkin_set_InputFieldSelectedTextColor_m9E6F7796FD5ECD0A18DC3B62F6C6123D696D682E (void);
// 0x0000014B UnityEngine.Color SimpleFileBrowser.UISkin::get_InputFieldCaretColor()
extern void UISkin_get_InputFieldCaretColor_mF94C7F5BAECD745F3955C0B4D348A16DEC4CE97C (void);
// 0x0000014C System.Void SimpleFileBrowser.UISkin::set_InputFieldCaretColor(UnityEngine.Color)
extern void UISkin_set_InputFieldCaretColor_mA5E1845D1927B4CDDF41C004664F706DF373E02A (void);
// 0x0000014D UnityEngine.Sprite SimpleFileBrowser.UISkin::get_InputFieldBackground()
extern void UISkin_get_InputFieldBackground_m9E4B1FEB7DBCE7B3C6C3158B98D63335925407FF (void);
// 0x0000014E System.Void SimpleFileBrowser.UISkin::set_InputFieldBackground(UnityEngine.Sprite)
extern void UISkin_set_InputFieldBackground_m30454DC4852F1B8A716AF1836B6CF10411AFFAAA (void);
// 0x0000014F UnityEngine.Color SimpleFileBrowser.UISkin::get_ButtonColor()
extern void UISkin_get_ButtonColor_m4CED62E6272B26B0D6C98534DB4313E467D323C9 (void);
// 0x00000150 System.Void SimpleFileBrowser.UISkin::set_ButtonColor(UnityEngine.Color)
extern void UISkin_set_ButtonColor_mAAC143C605A6FC5FF81FC2513E975B7145A491E4 (void);
// 0x00000151 UnityEngine.Color SimpleFileBrowser.UISkin::get_ButtonTextColor()
extern void UISkin_get_ButtonTextColor_mA762705039E71DD73F2C9BBCE7DCF7E833BA0E3B (void);
// 0x00000152 System.Void SimpleFileBrowser.UISkin::set_ButtonTextColor(UnityEngine.Color)
extern void UISkin_set_ButtonTextColor_m2A6B52496D891249BA32642DFDAC7E405E09A203 (void);
// 0x00000153 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_ButtonBackground()
extern void UISkin_get_ButtonBackground_mB1D404AEED0B9774F30D674BD202CF1428A6A892 (void);
// 0x00000154 System.Void SimpleFileBrowser.UISkin::set_ButtonBackground(UnityEngine.Sprite)
extern void UISkin_set_ButtonBackground_mF775247FC33D64F36A787FC4F9C36CD829EB2DA8 (void);
// 0x00000155 UnityEngine.Color SimpleFileBrowser.UISkin::get_DropdownColor()
extern void UISkin_get_DropdownColor_mDF1DC9234C2695E1FE3F39CA35756F2B5DA7E42E (void);
// 0x00000156 System.Void SimpleFileBrowser.UISkin::set_DropdownColor(UnityEngine.Color)
extern void UISkin_set_DropdownColor_m1DC7F7EDD31E2A96C646FA2915E0278857C9AA1E (void);
// 0x00000157 UnityEngine.Color SimpleFileBrowser.UISkin::get_DropdownTextColor()
extern void UISkin_get_DropdownTextColor_mBD5EDBD3E7DA244242139EA73EFB51AFB94B6E8D (void);
// 0x00000158 System.Void SimpleFileBrowser.UISkin::set_DropdownTextColor(UnityEngine.Color)
extern void UISkin_set_DropdownTextColor_mC6878CCF4E7016610DC10890EDB72D171D8404A7 (void);
// 0x00000159 UnityEngine.Color SimpleFileBrowser.UISkin::get_DropdownArrowColor()
extern void UISkin_get_DropdownArrowColor_m0BAA9A62C446917A1588EB59F236FF2F5E1B7680 (void);
// 0x0000015A System.Void SimpleFileBrowser.UISkin::set_DropdownArrowColor(UnityEngine.Color)
extern void UISkin_set_DropdownArrowColor_mAAC237A078DE523909FD5B563EEF0494F4B602E9 (void);
// 0x0000015B UnityEngine.Color SimpleFileBrowser.UISkin::get_DropdownCheckmarkColor()
extern void UISkin_get_DropdownCheckmarkColor_mC51672265D4B4812EF665A7A272B534A315AD76A (void);
// 0x0000015C System.Void SimpleFileBrowser.UISkin::set_DropdownCheckmarkColor(UnityEngine.Color)
extern void UISkin_set_DropdownCheckmarkColor_m5E95085890C7AC2C1F7F6CF0C800AD9F05A8AD02 (void);
// 0x0000015D UnityEngine.Sprite SimpleFileBrowser.UISkin::get_DropdownBackground()
extern void UISkin_get_DropdownBackground_m92ADAE1BE5D404AF633C4E01F34FB3C8D3B5C585 (void);
// 0x0000015E System.Void SimpleFileBrowser.UISkin::set_DropdownBackground(UnityEngine.Sprite)
extern void UISkin_set_DropdownBackground_mD5D5F5D3C3EB668C1C417031155759174C42EC5D (void);
// 0x0000015F UnityEngine.Sprite SimpleFileBrowser.UISkin::get_DropdownArrow()
extern void UISkin_get_DropdownArrow_m5B3CCA38E12871ADBF5C0490D8FFC086B38D471F (void);
// 0x00000160 System.Void SimpleFileBrowser.UISkin::set_DropdownArrow(UnityEngine.Sprite)
extern void UISkin_set_DropdownArrow_m45A87A039D8B99588A59BC8BFA08B83B116CC970 (void);
// 0x00000161 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_DropdownCheckmark()
extern void UISkin_get_DropdownCheckmark_mDE2FA7C79BA64DFBE642D1FDE9D2ABBEAF92B194 (void);
// 0x00000162 System.Void SimpleFileBrowser.UISkin::set_DropdownCheckmark(UnityEngine.Sprite)
extern void UISkin_set_DropdownCheckmark_mF016CF62667E81CD19A3BF10880C6C3121C57E47 (void);
// 0x00000163 UnityEngine.Color SimpleFileBrowser.UISkin::get_ToggleColor()
extern void UISkin_get_ToggleColor_m9E7CC20BC528EA672166874622A88AEC017D7F0D (void);
// 0x00000164 System.Void SimpleFileBrowser.UISkin::set_ToggleColor(UnityEngine.Color)
extern void UISkin_set_ToggleColor_m9DB0C5918C8FF182490ADB1FF98D0030F308B58B (void);
// 0x00000165 UnityEngine.Color SimpleFileBrowser.UISkin::get_ToggleTextColor()
extern void UISkin_get_ToggleTextColor_m7A38BE81665D996189C75FD7FA164B5C9C24DEDA (void);
// 0x00000166 System.Void SimpleFileBrowser.UISkin::set_ToggleTextColor(UnityEngine.Color)
extern void UISkin_set_ToggleTextColor_m02734327FE5CE4776CD73FCBB81D06C6607E5BDF (void);
// 0x00000167 UnityEngine.Color SimpleFileBrowser.UISkin::get_ToggleCheckmarkColor()
extern void UISkin_get_ToggleCheckmarkColor_m3E3911F246186C227EF8058089B52C4F97511EDA (void);
// 0x00000168 System.Void SimpleFileBrowser.UISkin::set_ToggleCheckmarkColor(UnityEngine.Color)
extern void UISkin_set_ToggleCheckmarkColor_mF04576D2C1F3D3F880DEC8DB55FD3BAD5B287DFF (void);
// 0x00000169 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_ToggleBackground()
extern void UISkin_get_ToggleBackground_m68C9BDE25EA7EF5B704E88E049BC2568B446C34F (void);
// 0x0000016A System.Void SimpleFileBrowser.UISkin::set_ToggleBackground(UnityEngine.Sprite)
extern void UISkin_set_ToggleBackground_mFCAD87E2279264C9437719499842829923C87DF4 (void);
// 0x0000016B UnityEngine.Sprite SimpleFileBrowser.UISkin::get_ToggleCheckmark()
extern void UISkin_get_ToggleCheckmark_m45AD35D157929952F77BE0B187DCDD93AEFEF8E1 (void);
// 0x0000016C System.Void SimpleFileBrowser.UISkin::set_ToggleCheckmark(UnityEngine.Sprite)
extern void UISkin_set_ToggleCheckmark_mDC026395E75DDA862CCF91D45E8C1502E7BF07FC (void);
// 0x0000016D UnityEngine.Color SimpleFileBrowser.UISkin::get_ScrollbarBackgroundColor()
extern void UISkin_get_ScrollbarBackgroundColor_m2476D94D639B09446FAB8AAC1680E82EC622ED78 (void);
// 0x0000016E System.Void SimpleFileBrowser.UISkin::set_ScrollbarBackgroundColor(UnityEngine.Color)
extern void UISkin_set_ScrollbarBackgroundColor_m2CB9CBC9FC10970DDABF5EA840CAB5096481689A (void);
// 0x0000016F UnityEngine.Color SimpleFileBrowser.UISkin::get_ScrollbarColor()
extern void UISkin_get_ScrollbarColor_m06B0E4B4BEC11A0D56E10598DB5506B49D14133E (void);
// 0x00000170 System.Void SimpleFileBrowser.UISkin::set_ScrollbarColor(UnityEngine.Color)
extern void UISkin_set_ScrollbarColor_mC576252377DF2D2CBE5EB23F019422E7B5DD898D (void);
// 0x00000171 System.Single SimpleFileBrowser.UISkin::get_FileHeight()
extern void UISkin_get_FileHeight_m490761AA2FCD12C90DE5F023FC3353965867FDF8 (void);
// 0x00000172 System.Void SimpleFileBrowser.UISkin::set_FileHeight(System.Single)
extern void UISkin_set_FileHeight_m423EA00C6764B88317B6AA7FC57255C7FDD15E92 (void);
// 0x00000173 System.Single SimpleFileBrowser.UISkin::get_FileIconsPadding()
extern void UISkin_get_FileIconsPadding_m3F6E9AB23B917C2804ECE55F73FF7ADD7591F0D5 (void);
// 0x00000174 System.Void SimpleFileBrowser.UISkin::set_FileIconsPadding(System.Single)
extern void UISkin_set_FileIconsPadding_m1A2819BC27847BEC67008DE8CD3DAF21C95A9C80 (void);
// 0x00000175 UnityEngine.Color SimpleFileBrowser.UISkin::get_FileNormalBackgroundColor()
extern void UISkin_get_FileNormalBackgroundColor_mB3AB273132682E837F5BFFDFDB3AF207D4C84A8A (void);
// 0x00000176 System.Void SimpleFileBrowser.UISkin::set_FileNormalBackgroundColor(UnityEngine.Color)
extern void UISkin_set_FileNormalBackgroundColor_m11EED12CB23F0A0F25A99F02EE0AAA06D50A033F (void);
// 0x00000177 UnityEngine.Color SimpleFileBrowser.UISkin::get_FileAlternatingBackgroundColor()
extern void UISkin_get_FileAlternatingBackgroundColor_m9C9A101FE901B7A8E530ED22A1AC3F603184B7EF (void);
// 0x00000178 System.Void SimpleFileBrowser.UISkin::set_FileAlternatingBackgroundColor(UnityEngine.Color)
extern void UISkin_set_FileAlternatingBackgroundColor_m74BB407D9D0441C4256FB8BF96B8F8231C6F8FF5 (void);
// 0x00000179 UnityEngine.Color SimpleFileBrowser.UISkin::get_FileHoveredBackgroundColor()
extern void UISkin_get_FileHoveredBackgroundColor_mBBFA2C11C2DE553F634A40D8B2EFA414D8BED2C6 (void);
// 0x0000017A System.Void SimpleFileBrowser.UISkin::set_FileHoveredBackgroundColor(UnityEngine.Color)
extern void UISkin_set_FileHoveredBackgroundColor_m489C9E0B2E0EA776DF57F571949280E32689914B (void);
// 0x0000017B UnityEngine.Color SimpleFileBrowser.UISkin::get_FileSelectedBackgroundColor()
extern void UISkin_get_FileSelectedBackgroundColor_m3E275E244C9580296423FEB3C26DF2D30C44BF37 (void);
// 0x0000017C System.Void SimpleFileBrowser.UISkin::set_FileSelectedBackgroundColor(UnityEngine.Color)
extern void UISkin_set_FileSelectedBackgroundColor_m471F5A3700D8881BA34709EE9533639BEC5EB0CD (void);
// 0x0000017D UnityEngine.Color SimpleFileBrowser.UISkin::get_FileNormalTextColor()
extern void UISkin_get_FileNormalTextColor_m858F74B10CCAF4618CA6E5C7167BAB80217D0A99 (void);
// 0x0000017E System.Void SimpleFileBrowser.UISkin::set_FileNormalTextColor(UnityEngine.Color)
extern void UISkin_set_FileNormalTextColor_mAA436E5280042DF16ADEFBE949E0EB9599C28FE5 (void);
// 0x0000017F UnityEngine.Color SimpleFileBrowser.UISkin::get_FileSelectedTextColor()
extern void UISkin_get_FileSelectedTextColor_mC1E14053CCF0BD5E03EC173BB7188B4DD0795EB4 (void);
// 0x00000180 System.Void SimpleFileBrowser.UISkin::set_FileSelectedTextColor(UnityEngine.Color)
extern void UISkin_set_FileSelectedTextColor_m0F0EDD9CC86EC19F48FCFB4EE5F705047EA045AA (void);
// 0x00000181 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_FolderIcon()
extern void UISkin_get_FolderIcon_m0A907951175B18526FF527BDC3BDF52D06B3E6A4 (void);
// 0x00000182 System.Void SimpleFileBrowser.UISkin::set_FolderIcon(UnityEngine.Sprite)
extern void UISkin_set_FolderIcon_m05531A4BA6947BEDC85E60329490D2CF1FEAB87E (void);
// 0x00000183 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_DriveIcon()
extern void UISkin_get_DriveIcon_mBEEB8049F062992480B4CDE6D40762778981EB6A (void);
// 0x00000184 System.Void SimpleFileBrowser.UISkin::set_DriveIcon(UnityEngine.Sprite)
extern void UISkin_set_DriveIcon_m525AB2878135C5B877DBC8EF7F2CE689C42D71F0 (void);
// 0x00000185 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_DefaultFileIcon()
extern void UISkin_get_DefaultFileIcon_m93D7C9CF7C1D84FD464C0F92FA9CE0F3589A9A29 (void);
// 0x00000186 System.Void SimpleFileBrowser.UISkin::set_DefaultFileIcon(UnityEngine.Sprite)
extern void UISkin_set_DefaultFileIcon_m4FE8383F04CD0B27B3B3C6EE7849F4D5EC929D65 (void);
// 0x00000187 SimpleFileBrowser.FiletypeIcon[] SimpleFileBrowser.UISkin::get_FiletypeIcons()
extern void UISkin_get_FiletypeIcons_mE72BFF3BDC905792B9447680D7697683EDE77D0E (void);
// 0x00000188 System.Void SimpleFileBrowser.UISkin::set_FiletypeIcons(SimpleFileBrowser.FiletypeIcon[])
extern void UISkin_set_FiletypeIcons_mB29C8314A4C4979F4CEC787DE8B8E5C560A8708F (void);
// 0x00000189 System.Boolean SimpleFileBrowser.UISkin::get_AllIconExtensionsHaveSingleSuffix()
extern void UISkin_get_AllIconExtensionsHaveSingleSuffix_m5C42381A1E46E9E52FFB01793EFC8B5C5055AEE4 (void);
// 0x0000018A UnityEngine.Sprite SimpleFileBrowser.UISkin::get_FileMultiSelectionToggleOffIcon()
extern void UISkin_get_FileMultiSelectionToggleOffIcon_m03AB5F1FC3C182BBB9D891A48A74236D419AF857 (void);
// 0x0000018B System.Void SimpleFileBrowser.UISkin::set_FileMultiSelectionToggleOffIcon(UnityEngine.Sprite)
extern void UISkin_set_FileMultiSelectionToggleOffIcon_m8CA945920706E530712B2333CE608BE59BB6A2FA (void);
// 0x0000018C UnityEngine.Sprite SimpleFileBrowser.UISkin::get_FileMultiSelectionToggleOnIcon()
extern void UISkin_get_FileMultiSelectionToggleOnIcon_mD78F39CDD08F1D545BAB8F5206634F0D0FBE395C (void);
// 0x0000018D System.Void SimpleFileBrowser.UISkin::set_FileMultiSelectionToggleOnIcon(UnityEngine.Sprite)
extern void UISkin_set_FileMultiSelectionToggleOnIcon_mD081AC1D7929EAB46898CA3C74548B0E567D15B1 (void);
// 0x0000018E UnityEngine.Color SimpleFileBrowser.UISkin::get_ContextMenuBackgroundColor()
extern void UISkin_get_ContextMenuBackgroundColor_mB22BB4E4E1F1174790B7C5155A6A56C19ECBB545 (void);
// 0x0000018F System.Void SimpleFileBrowser.UISkin::set_ContextMenuBackgroundColor(UnityEngine.Color)
extern void UISkin_set_ContextMenuBackgroundColor_mE4D66E88E0AD25792D579BCF36D1E0FCDA140F52 (void);
// 0x00000190 UnityEngine.Color SimpleFileBrowser.UISkin::get_ContextMenuTextColor()
extern void UISkin_get_ContextMenuTextColor_m7442C31C4B35D1CC6BF57F8BB91C4C7AABE6E76A (void);
// 0x00000191 System.Void SimpleFileBrowser.UISkin::set_ContextMenuTextColor(UnityEngine.Color)
extern void UISkin_set_ContextMenuTextColor_m2BB01FB8EF5D6AAAC9755960FFE9D53CC408283F (void);
// 0x00000192 UnityEngine.Color SimpleFileBrowser.UISkin::get_ContextMenuSeparatorColor()
extern void UISkin_get_ContextMenuSeparatorColor_mD624EE6AEA7EB9986A1A0EBA099A218D9FDEB8B9 (void);
// 0x00000193 System.Void SimpleFileBrowser.UISkin::set_ContextMenuSeparatorColor(UnityEngine.Color)
extern void UISkin_set_ContextMenuSeparatorColor_m422511782E3018925AD56EFCC8E54E94572B3232 (void);
// 0x00000194 UnityEngine.Color SimpleFileBrowser.UISkin::get_DeletePanelBackgroundColor()
extern void UISkin_get_DeletePanelBackgroundColor_m2BDD89F76C36F336A4EAB7CD1009A04842B6F1BA (void);
// 0x00000195 System.Void SimpleFileBrowser.UISkin::set_DeletePanelBackgroundColor(UnityEngine.Color)
extern void UISkin_set_DeletePanelBackgroundColor_mD0C05EAA0D6E4D50394E17ED16F004B9E58F04F8 (void);
// 0x00000196 UnityEngine.Color SimpleFileBrowser.UISkin::get_DeletePanelTextColor()
extern void UISkin_get_DeletePanelTextColor_m43772B841F7B988833F46172DDC5EF0C9C2674EE (void);
// 0x00000197 System.Void SimpleFileBrowser.UISkin::set_DeletePanelTextColor(UnityEngine.Color)
extern void UISkin_set_DeletePanelTextColor_m61E85E92C56B748A37F09414120B040791699751 (void);
// 0x00000198 UnityEngine.Sprite SimpleFileBrowser.UISkin::get_DeletePanelBackground()
extern void UISkin_get_DeletePanelBackground_m737D4F769A76B919552F437BA9625ACBBDF392B4 (void);
// 0x00000199 System.Void SimpleFileBrowser.UISkin::set_DeletePanelBackground(UnityEngine.Sprite)
extern void UISkin_set_DeletePanelBackground_m3F1A71803331D1F0D2445B83D4AF3668BBDF1B05 (void);
// 0x0000019A System.Void SimpleFileBrowser.UISkin::ApplyTo(UnityEngine.UI.Text,UnityEngine.Color)
extern void UISkin_ApplyTo_m53C98C362E95A9E5E22E385E0A39FEAFA2DA5DEB (void);
// 0x0000019B System.Void SimpleFileBrowser.UISkin::ApplyTo(UnityEngine.UI.InputField)
extern void UISkin_ApplyTo_m9B342028BE704955C675CC93864D4347FCF282BA (void);
// 0x0000019C System.Void SimpleFileBrowser.UISkin::ApplyTo(UnityEngine.UI.Button)
extern void UISkin_ApplyTo_m74872228D165B123D86A4E2692C2027E337A6602 (void);
// 0x0000019D System.Void SimpleFileBrowser.UISkin::ApplyTo(UnityEngine.UI.Dropdown)
extern void UISkin_ApplyTo_mD21874F09929E40C271F99AB71EA5EF2B91FD354 (void);
// 0x0000019E System.Void SimpleFileBrowser.UISkin::ApplyTo(UnityEngine.UI.Toggle)
extern void UISkin_ApplyTo_m90BBC4AB07D33B0EDD572D2CD6A92F52C353F493 (void);
// 0x0000019F System.Void SimpleFileBrowser.UISkin::ApplyTo(UnityEngine.UI.Scrollbar)
extern void UISkin_ApplyTo_m82C9BDB7381611FFF8AE75C42E41824F0E4E8E72 (void);
// 0x000001A0 UnityEngine.Sprite SimpleFileBrowser.UISkin::GetIconForFileEntry(SimpleFileBrowser.FileSystemEntry,System.Boolean)
extern void UISkin_GetIconForFileEntry_mBE3548DA513A0F0D809AFD07DC7067086E53E5AC (void);
// 0x000001A1 System.Void SimpleFileBrowser.UISkin::InitializeFiletypeIcons()
extern void UISkin_InitializeFiletypeIcons_mA6B13A4B8D906480ABED33D8C70959FB25F94143 (void);
// 0x000001A2 System.Void SimpleFileBrowser.UISkin::.ctor()
extern void UISkin__ctor_m47C84F2C0A2F9BC4C013283F192B24251FD4340C (void);
static Il2CppMethodPointer s_methodPointers[418] = 
{
	FileBrowser_get_IsOpen_m18814451BEC8E8CADB599BC8F23E6C37104BE50C,
	FileBrowser_set_IsOpen_mC94644F22A61409CE23169AAD18499301862D444,
	FileBrowser_get_Success_m5809EE68C776F1500213DFFAC167325DC56F879C,
	FileBrowser_set_Success_mE3E30E699935AC3FC78C668691DDB21610E1F1B4,
	FileBrowser_get_Result_m68BED71300B0738904437796A2C20ADB8B057FF0,
	FileBrowser_set_Result_m6656F9251FE3557D72BF0D0F4BA83B475820F8AD,
	FileBrowser_get_Skin_mF5E5790CDB02A70F885B319526F5FA5C8B4A518F,
	FileBrowser_set_Skin_m66738BFFEE37F4AB8939538A6D4BDA3738EE3916,
	FileBrowser_get_AskPermissions_m550ED3B674566CE6C04CB7F76C97B07686BFCA16,
	FileBrowser_set_AskPermissions_mC267C7AD7FB68495D67E5AF6B9979DEA746A66C9,
	FileBrowser_get_SingleClickMode_mBC1D95A4EF5641CE3D6B8096C609EE0467F03C7D,
	FileBrowser_set_SingleClickMode_m180E49862F045FAE51B006F800C7EB01E393D29D,
	FileBrowser_add_DisplayedEntriesFilter_m7589F78FDCCB84B18E9F961CEE69913F64AB5E26,
	FileBrowser_remove_DisplayedEntriesFilter_m03ECE3627146AAFFB166553186578B79767CA0F8,
	FileBrowser_get_DrivesRefreshInterval_m758A3C4DDE154BD4BDD1C29660FEDB0C5D359E41,
	FileBrowser_set_DrivesRefreshInterval_m272B5E8EB171918BAF26302F29847A276284EE3F,
	FileBrowser_get_ShowHiddenFiles_m56B300897CACD137E41BA49D4966B0D5ACCF6D60,
	FileBrowser_set_ShowHiddenFiles_mAFC629F52C31CA3C826D1699C1A32F8D7C03D57D,
	FileBrowser_get_DisplayHiddenFilesToggle_mF2F1261B15DF7A8A9467CBA2D9A4D9CF7E21B1C0,
	FileBrowser_set_DisplayHiddenFilesToggle_mE7BDB07034AC4C92162E4CF763F4DA9DC683ED3A,
	FileBrowser_get_AllFilesFilterText_m11A59EC77A38E7954AD443B9BC9B445468BDB842,
	FileBrowser_set_AllFilesFilterText_mDE2F45DE0DE0CE7F7074ABE3CB39180485B215E8,
	FileBrowser_get_FoldersFilterText_mA3D49D59F8F4FB8D7DD400B783872F0505785FA5,
	FileBrowser_set_FoldersFilterText_m159DFA629E9C095D04F97A11A5EE79B87D021BA0,
	FileBrowser_get_PickFolderQuickLinkText_mBEFAA93F33C6344716A60C7DFDD4B5752AE7CBEF,
	FileBrowser_set_PickFolderQuickLinkText_m5C463A1BF7E16C97302082D49C87176EFC6CFF6D,
	FileBrowser_get_Instance_mB99D0C8AFF82F3377E62304CFB0C9210F2710DD4,
	FileBrowser_get_AllExtensionsHaveSingleSuffix_mE1B6D3A19497EB0952FAC108AA6DCD46A63AC3A2,
	FileBrowser_get_CurrentPath_mBFA8E36877B8A5F0817EADA032344219B8207E4F,
	FileBrowser_set_CurrentPath_mC77AFECC7927FE02B265B490C3825EEE685E3BE7,
	FileBrowser_get_SearchString_mE8FD1C23E5E06B45B4A3E0082ECC671E7F186EF6,
	FileBrowser_set_SearchString_mDDD0C4D50DA3999E4338C0A8A001279D08331E48,
	FileBrowser_get_AcceptNonExistingFilename_mA1E6CAA75D40889BEAA0B528C86DBBF10841913F,
	FileBrowser_set_AcceptNonExistingFilename_mF0E53CE81D674BEBE2583049BEE0586661374079,
	FileBrowser_get_PickerMode_m1E7F1A135BE57C9B4E56097009BC001E2CCDF616,
	FileBrowser_set_PickerMode_m252A1D099DEF70FAF8AC72FD5D7A4C45B65AA206,
	FileBrowser_get_AllowMultiSelection_m3CE75D1DD5A555180373AFE8E446D298F9CCB97C,
	FileBrowser_set_AllowMultiSelection_m4FC44DD7C7836F9F094C0CBE960A29259B0D1E05,
	FileBrowser_get_MultiSelectionToggleSelectionMode_m0CC81C607E6309B5E0E706E5A8F74CF3E541A082,
	FileBrowser_set_MultiSelectionToggleSelectionMode_m0F58D2FE698D3B849FB78DC405551C2A54899830,
	FileBrowser_get_Title_mB4AB85A4B858B6CE7671AE32D30D7EE997CEA8B3,
	FileBrowser_set_Title_mA303A37D55D2D07C43D734B10507A42B55D10423,
	FileBrowser_get_SubmitButtonText_m4E82C1110DB829DD32BA479A3D5337D1659D2D0C,
	FileBrowser_set_SubmitButtonText_m29CFCD8B22FB378B3E8F4388C72C7A1A7AB3C0D4,
	FileBrowser_get_LastBrowsedFolder_m6457C65E0801437674EF2AC7C3732368ACB70438,
	FileBrowser_set_LastBrowsedFolder_m74581F03A4ADFF9A990FD5F467A1E8978DF7A46A,
	FileBrowser_Awake_mE271079885CA728F72B3F73D83CE78DC5880C23A,
	FileBrowser_OnRectTransformDimensionsChange_mE4DAC20591CBB27C5A5A2F97A66E86FCB931A9B3,
	FileBrowser_Update_m3F1F142729B656AC489C8AA7307E174CBC54C565,
	FileBrowser_LateUpdate_mA3374004B750A9D6FF0F4D11752DF0FC88B32235,
	FileBrowser_OnApplicationFocus_m0D8C1F78585868840DDE556454B4D22A766E861E,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_get_OnItemClicked_m9126447B8863E925FD5D3B2CF60188BD993FC9F5,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_set_OnItemClicked_m202B2600CCE6D8BC02F6CD6929C065B0F2EFF270,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_get_Count_m0AC6A73A9EBA0573CFABED865682EF679CF79D95,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_get_ItemHeight_m0577A36A99AE4760ADCE32284B245268909CC2C1,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_CreateItem_m3A94D4404F711861B1A9CE82DD08DC27B239D09A,
	FileBrowser_SimpleFileBrowser_IListViewAdapter_SetItemContent_mF1D94F26886A961DEAA8528D76625CAEF0812F9A,
	FileBrowser_InitializeQuickLinks_mC08CA308D9034BA498A4CE1AC3FFE849230DFB56,
	FileBrowser_RefreshDriveQuickLinks_m540D5683EDD68826961F7814C499450EB2E14798,
	FileBrowser_RefreshSkin_mB4CF280DFD76B5963B1957FDC4C5D1C7630606DC,
	FileBrowser_OnBackButtonPressed_mAD0E3CAB0818551D542A9516897E1B1496B44968,
	FileBrowser_OnForwardButtonPressed_mFE46E8ECD432849A423C848FB7D39328CB87EC62,
	FileBrowser_OnUpButtonPressed_mCAA26ECCA4B13284A4B7E4D977A388ACCF1AFB9E,
	FileBrowser_OnMoreOptionsButtonClicked_m10E401F7C1C6F202F1848A96DA25AECFA6D949C7,
	FileBrowser_OnContextMenuTriggered_mFA0AA158FE9AE9AD8ECB0D3DEB8D169481E02319,
	FileBrowser_ShowContextMenuAt_m13F6C5767CB0B266FF2F2D583C23957BFC74453C,
	FileBrowser_OnSubmitButtonClicked_m3332294C34AC603145DA335F0C86BD11A0688DF8,
	FileBrowser_OnCancelButtonClicked_m48955553F8A90376E93A23112E8ECCCC2CD82954,
	FileBrowser_OnOperationSuccessful_m2CF8A90FC6BA08A1765667852A41111CF43FB4DB,
	FileBrowser_OnOperationCanceled_mDC4CDB908EE1BBC64F34DC02D56BF6B9746F78D8,
	FileBrowser_OnPathChanged_mA276DEEBF8BF2572D763A15AA21036216B996661,
	FileBrowser_OnSearchStringChanged_m06A1A24E20B3BA543E3F644CFE49D682AF810BA2,
	FileBrowser_OnFilterChanged_m04F962445E850063CD19D87F1DED4B9962BCC0A7,
	FileBrowser_OnShowHiddenFilesToggleChanged_m72833F478BA724C4D32C89EC1AD44D1F9D1C478F,
	FileBrowser_OnItemSelected_m553F21D12D4F28911E2B04E2B5E14D434AE8B067,
	FileBrowser_OnItemHeld_m4616B0F779C71648811095E004E6852065EC8F70,
	FileBrowser_OnValidateFilenameInput_m12AF80C6D178EDAD4462D1F8E4AA4FA03D79D02A,
	FileBrowser_OnFilenameInputChanged_m03FC49F2400775036127FF42D4E27568809CE1B7,
	FileBrowser_Show_m64A9C6492FE4F3A2E083847F85078AF64A491C98,
	FileBrowser_Hide_mD14D207CDCAEA3EBCE749396FA71BF049BC897A1,
	FileBrowser_RefreshFiles_m14E4ABD98486CB7DDAD461D233295B6DA37ED763,
	FileBrowser_SelectAllFiles_m44CC4BEA6B91DF34E635049598E4E5FFFF45E8B1,
	FileBrowser_DeselectAllFiles_m77712CDD088C8976A9D19FDE678E6712C4BB3AA4,
	FileBrowser_CreateNewFolder_m134E6B000CC231D49ADE213F46A4FC0B43C63E43,
	FileBrowser_CreateNewFolderCoroutine_mB8A66589D4DCF5D8ECA7C2EA681101BD9D17CABD,
	FileBrowser_RenameSelectedFile_m81CA56F0C89D20230E7A22C487D7B914E45582CF,
	FileBrowser_DeleteSelectedFiles_mCA7847F4540F0B4DBEF0885A3C692F9F0A0562FE,
	FileBrowser_PersistFileEntrySelection_m539A9413D6681B3BA6FE5A474F255CD1F282CA93,
	FileBrowser_AddQuickLink_mF254B02358535E14D2319F1AC1230F513D58B42A,
	FileBrowser_ClearQuickLinksInternal_m16606512E3CA993A619C864CC764D9253FBAE162,
	FileBrowser_EnsureScrollViewIsWithinBounds_mB014E75CA8EFC7FC5B9A388413D610BE6747D954,
	FileBrowser_EnsureWindowIsWithinBounds_m6052BE3802013AF0A0F003D19C6A132E144F3EE6,
	FileBrowser_OnWindowDimensionsChanged_mF3F9FE60AFAAD85235F6AF5E5F6E28530C397A0C,
	FileBrowser_GetIconForFileEntry_m39E97FDC81C7D26CDC1A09146CCB8D50949FD147,
	FileBrowser_GetExtensionFromFilename_mDFAD3BC2729A0685EDCD8D46085C755670E02346,
	FileBrowser_GetPathWithoutTrailingDirectorySeparator_m56DF6CC3E788CE233690E42B4878B6D45E5F4B21,
	FileBrowser_UpdateFilenameInputFieldWithSelection_mE68C8FA9B60ADB869F43D6FD7D8ABA5301A7F7E1,
	FileBrowser_ExtractFilenameFromInput_m8D87F77E24E8BCCC89D886486CAE777A7848FADD,
	FileBrowser_FilenameInputToFileEntryIndex_m8EEFD8C4E92482B7D67428C5B2719E9397A9F03E,
	FileBrowser_VerifyFilenameInput_m8533205E3B8F0854F81075FBE36C1705CEDFEEF5,
	FileBrowser_CalculateLengthOfDropdownText_m9066BB1F5AB9C6051690469BD3FFA52BC20ADD9D,
	FileBrowser_GetInitialPath_mB6A20DD2C4B53CA447385B11A12FDA952FFB5831,
	FileBrowser_ShowSaveDialog_m144B1D73495975776018A3D243D9C5902CAA8B2C,
	FileBrowser_ShowLoadDialog_mAE177552745B54980984B3EBE1EB9830B79DC082,
	FileBrowser_ShowDialogInternal_mC92646F8C1B0FF0AD77C57F9D0EBEEDE2BB07ADE,
	FileBrowser_HideDialog_m02BE36614EF3CEFE0F3D41E44EBA950427055811,
	FileBrowser_WaitForSaveDialog_m3190752768E451A01CAE16E47FAFE4E531E96EA2,
	FileBrowser_WaitForLoadDialog_m5ABC9FC7FDD5C18B32153309C0204E5862C584B9,
	FileBrowser_AddQuickLink_m5FA47D097233AC295433BC44263C91A4ED6F29B9,
	FileBrowser_ClearQuickLinks_m30154506F3DA9285139828125C0C8EDC84415462,
	FileBrowser_SetExcludedExtensions_mF4555FEA87743C688BF75C4A90B2CC9BB80547A5,
	FileBrowser_SetFilters_m65982A252AE3CB8F23BA581A64D017E4198AABD8,
	FileBrowser_SetFilters_m9241687743642DA21496022FF91E789BF00AB257,
	FileBrowser_SetFilters_m9A1F7D8B946BE18B89EC1FEBE09E68C227EEF5A0,
	FileBrowser_SetFilters_mE9AC44A5A36311CBB2A367869FC01C5E62E2001E,
	FileBrowser_SetFilters_mDAFF9AD6984CF9B471A02570ABAE4967A7C70853,
	FileBrowser_SetFiltersPreProcessing_m06AA25B9394BC9012763EA01B0EB166C8BDBEE42,
	FileBrowser_SetFiltersPostProcessing_m026D4142F0AACA8173F861F5A0E86969EBB01C7C,
	FileBrowser_SetDefaultFilter_m85E9F64B38B73DD95159420FFF396D50C2084B2D,
	FileBrowser_CheckPermission_mB59E9671407F61B85EA45F21A7F69C289E7FB7B4,
	FileBrowser_RequestPermission_m7F419FBC909B5904989D292C92427ACB414B489E,
	FileBrowser__ctor_mA14C08F72ED0CDE1E8DDDD71EE3194A89AA4A5D5,
	FileBrowser__cctor_m8B60B9C9A649551158D47C24E68D2225BA5CBC88,
	FileBrowser_U3CCreateNewFolderCoroutineU3Eb__225_0_mB6EC002D40146C5504A129B8A05B3CF47C0A58E8,
	FileBrowser_U3CDeleteSelectedFilesU3Eb__227_0_m32D69DCD61D56C2D0D1ABAA04FE41E4D358FC742,
	Filter__ctor_m1A2EDFD7D15168A8C473A0F6760D6643200DCDBE,
	Filter__ctor_m26233F843105D6920F34365B6BD6B82BFA9FB44E,
	Filter__ctor_m0082D963A2376632C2CEAC00B337B8133F53FD29,
	Filter_MatchesExtension_m599F1975211ADE2667EC0B6AE513A1451153042E,
	Filter_ToString_mDE0DACA728B5CAFBC6C1828D9BCF81440700728E,
	OnSuccess__ctor_mAED1943D7906AE017191AFC0DC639FCD5B016D25,
	OnSuccess_Invoke_m4067B92437B334D4939C8A18956DB1855DAD060B,
	OnSuccess_BeginInvoke_mC2A00A176DF5240F86FFF69E73BCBCAE4A3641E2,
	OnSuccess_EndInvoke_mFBB23F394B914823AEBF9ABA3FA0490A12305CE1,
	OnCancel__ctor_m26ACFA25E1D02F85A27B166AF42CFCFBFFD0772C,
	OnCancel_Invoke_mE143A09B94A5BA1EC2E849F9442BA582C0A74361,
	OnCancel_BeginInvoke_mE5AFDA66A648E987B9AF356AF0535A93F31FF8FA,
	OnCancel_EndInvoke_m3E2A8577E6F859560F80CEB507DB1A73A9B8987D,
	FileSystemEntryFilter__ctor_m3869D23A38762C2C87A6B728CFEBBEADB8FF817E,
	FileSystemEntryFilter_Invoke_m9516A832F92C0BC36EBFC9EB938002D9663B1ABA,
	FileSystemEntryFilter_BeginInvoke_m705E174CB6DADA9AEB3174922D9B32752F12489B,
	FileSystemEntryFilter_EndInvoke_m31005911F9DC628C5979B063494F67830D48D611,
	U3CU3Ec__cctor_m907FDC5153760B702813F89A1403512782C27FCD,
	U3CU3Ec__ctor_mDC67BB8C9A0B1001B8C37486C2AA5D9197175C1A,
	U3CU3Ec_U3CRefreshFilesU3Eb__221_0_m7F4E253E3CC966C2CF3324DEE33C8222202518EA,
	U3CCreateNewFolderCoroutineU3Ed__225__ctor_m67A83A599B1CA7186A4E09F28F8D02D33FBFE9CD,
	U3CCreateNewFolderCoroutineU3Ed__225_System_IDisposable_Dispose_m44BE0222F4CACCAE308A6D9CAF1E3D644A31E088,
	U3CCreateNewFolderCoroutineU3Ed__225_MoveNext_m7BAFA8B04EE2B6E82B64B51FE419A0CF99A21478,
	U3CCreateNewFolderCoroutineU3Ed__225_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD7D6930A8458B2785DF415AF2491A582AA8E42D3,
	U3CCreateNewFolderCoroutineU3Ed__225_System_Collections_IEnumerator_Reset_m631BB4979996667E05FCDD3758DB586026B69946,
	U3CCreateNewFolderCoroutineU3Ed__225_System_Collections_IEnumerator_get_Current_m0434987E41C9DB04860419E9DD9EE7941522918E,
	U3CU3Ec__DisplayClass226_0__ctor_mE630ED777201A655FB478F9CD988B0C8883B2040,
	U3CU3Ec__DisplayClass226_0_U3CRenameSelectedFileU3Eb__0_mE665142043B8931B13FC1F0C274813774EDFB86B,
	U3CWaitForSaveDialogU3Ed__247__ctor_m495B2CF322F9A4C6C7C46B34BB6A86E1CB8AB39C,
	U3CWaitForSaveDialogU3Ed__247_System_IDisposable_Dispose_mBAC79E1622A6000A1364A57AF2590C98D24C55D1,
	U3CWaitForSaveDialogU3Ed__247_MoveNext_m5780A6EB2FF85EEACCA951DB9F7089BEF266F877,
	U3CWaitForSaveDialogU3Ed__247_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m391D053B3B9F791F26CABB1F0F8617663C197226,
	U3CWaitForSaveDialogU3Ed__247_System_Collections_IEnumerator_Reset_m6FEC308614348E262E8DA756DA8F12A00CD00286,
	U3CWaitForSaveDialogU3Ed__247_System_Collections_IEnumerator_get_Current_m9661EBF33B737CE78BC0EF76F15B83A1FE80FF42,
	U3CWaitForLoadDialogU3Ed__248__ctor_m1D1695204E2EA535969FBD490ECA046E08044AB2,
	U3CWaitForLoadDialogU3Ed__248_System_IDisposable_Dispose_m81CD0AFC6D088F80A7E858125D0E821772252300,
	U3CWaitForLoadDialogU3Ed__248_MoveNext_m146AAAF1BE30DF27CD8099CD057F7734D70EDBE7,
	U3CWaitForLoadDialogU3Ed__248_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1752A1CEA7D63E5E11485504CF1FFC7DCDC0EE8,
	U3CWaitForLoadDialogU3Ed__248_System_Collections_IEnumerator_Reset_mEE6C5A4B06D6FF06A180A53E7AE171B00566BD2F,
	U3CWaitForLoadDialogU3Ed__248_System_Collections_IEnumerator_get_Current_m839B958CDCD2A6B0C33718FCA76B880C082C27C7,
	FileBrowserContextMenu_Show_mA733F9163F6D4929AF02FF90393B092D6A1BDCD8,
	FileBrowserContextMenu_Hide_m4F44530197AA791474C6EB6DA5AE8769537F3B0C,
	FileBrowserContextMenu_RefreshSkin_mA4DA4B81E3CD5F07DAC9C1C03812E99F8517E58F,
	FileBrowserContextMenu_OnSelectAllButtonClicked_m69A4007CE3F55DB2B417588BE76C36D91BE89E14,
	FileBrowserContextMenu_OnDeselectAllButtonClicked_mB3184AE7626029BAFD1FF6421DD1C017931B8599,
	FileBrowserContextMenu_OnCreateFolderButtonClicked_m02009A1FD862FC3F0F9DBCC20EDFEA1BD11AF5C9,
	FileBrowserContextMenu_OnDeleteButtonClicked_m8658E87083F4D963BC3B705E60CD2E11EC211A99,
	FileBrowserContextMenu_OnRenameButtonClicked_m9001A3AC6E30F35E99624014C923C63EC173EB19,
	FileBrowserContextMenu__ctor_mD48F391D8AE027F325EB80BDB8033012AAB83C04,
	FileBrowserCursorHandler_UnityEngine_EventSystems_IPointerEnterHandler_OnPointerEnter_mD3E85EB94541AC1352CF0267D031B058FA9B3731,
	FileBrowserCursorHandler_UnityEngine_EventSystems_IPointerExitHandler_OnPointerExit_mC51857736F09A3F196DBB341D877B977B6F300E5,
	FileBrowserCursorHandler_UnityEngine_EventSystems_IBeginDragHandler_OnBeginDrag_m3E295A9B3006C9B59750719B81C8F58D517D328C,
	FileBrowserCursorHandler_UnityEngine_EventSystems_IEndDragHandler_OnEndDrag_m2871B399DF1FB6D0C079F8A2E6621D917FF3CA14,
	FileBrowserCursorHandler_ShowDefaultCursor_mC705BE56E9F59A8124D332B831AF6F5D8AC1FFBE,
	FileBrowserCursorHandler_ShowResizeCursor_m4FD081FBE47A419227A5032A8D065B60ECB6EB32,
	FileBrowserCursorHandler__ctor_mE7AF5F81DBF3EB93910E74617D29B4C5C62A728A,
	FileBrowserDeleteConfirmationPanel_Show_m67A714F441AE140570CF68216DD4BB607EAEE0F4,
	FileBrowserDeleteConfirmationPanel_OnCanvasDimensionsChanged_mEBED06B7C2B79D72BB46D075BA9623E236568C78,
	FileBrowserDeleteConfirmationPanel_RefreshSkin_mA988F9B82D73A78E3357655388A014D7F1DC8218,
	FileBrowserDeleteConfirmationPanel_YesButtonClicked_mD4E097A0309A4F375F29CA4A84A04B17C8D9C06C,
	FileBrowserDeleteConfirmationPanel_NoButtonClicked_m06E216DACB4F48FF98117633CE5046C2A53D84BD,
	FileBrowserDeleteConfirmationPanel__ctor_m7FD9E661764ED42230B0FE31232F0F23AB464B81,
	OnDeletionConfirmed__ctor_m6D56888CB1E3F037AD52E31DCBA82ACE8A94BADF,
	OnDeletionConfirmed_Invoke_mA3D4CA80D75F726B8B66054C3BD58CF213B4338B,
	OnDeletionConfirmed_BeginInvoke_mC621E4FD8F54E439AB1F41CEDAE33C0E2DE3345C,
	OnDeletionConfirmed_EndInvoke_m9F6DB6C94C05198F5F7F746FDF610E37BF40B0B5,
	FileSystemEntry_get_IsDirectory_m5B6EAD6DAC01DC97FA18BF8C82FB57CD009F9E53,
	FileSystemEntry__ctor_mFB6D7113F6EC140A0863D55B690ACADC5A22EBD1,
	FileSystemEntry__ctor_mF3DAAE9ECE05D268F76C8A7A8E9B23083B699C22,
	FileBrowserHelpers_FileExists_mD73A1D794A8CCB271F1BA746A082354E338F57FD,
	FileBrowserHelpers_DirectoryExists_m34D0C75C33EABE4964762FC0C9B8B8446FF6C335,
	FileBrowserHelpers_IsDirectory_m2E0623A10A6163784CF86F92DF6345C7AB2C8650,
	FileBrowserHelpers_GetDirectoryName_m6342C65603EF0B25E2C2382773027263E5AB1817,
	FileBrowserHelpers_GetEntriesInDirectory_m4E5FA288844CAA15C783E32EB39CD6C062B37AC3,
	FileBrowserHelpers_CreateFileInDirectory_m494CB83E0BB762403696BF033B728AD1D43C6477,
	FileBrowserHelpers_CreateFolderInDirectory_mAEE39C8930FB9F6A6972D87CE827CD8EA6C7B9A7,
	FileBrowserHelpers_WriteBytesToFile_m8044FC5F818D349E6B4AB56D2C7C71E76B1B9967,
	FileBrowserHelpers_WriteTextToFile_m296944773E9E3939C4985D8754D4C13E5599A706,
	FileBrowserHelpers_AppendBytesToFile_m156371AE42B0661E70DEDD24812F02A7096C9F95,
	FileBrowserHelpers_AppendTextToFile_m2C9CCC9F3D53F3B3FEDD5B899852239580ED2754,
	FileBrowserHelpers_AppendFileToFile_m22CEECB47EF2E23DAD9A2B472392AAAD66BF59F9,
	FileBrowserHelpers_ReadBytesFromFile_m6A2C4FF9E1A0805920FD1A10E41FE822DE6B2C21,
	FileBrowserHelpers_ReadTextFromFile_m29394418D7A8B9A7B66004DF8AC2E2C37B68C566,
	FileBrowserHelpers_CopyFile_mC01E97041A6955509A71728024D34BD35CD69C3B,
	FileBrowserHelpers_CopyDirectory_m99947742CE8BC2D9E71E1F419F36C87C788C4B4D,
	FileBrowserHelpers_CopyDirectoryRecursively_mC86E85947B557233487229377CC3F6BB6C3F00F0,
	FileBrowserHelpers_MoveFile_m6D1090980ED4B6A9C2C7510474E73495794FC978,
	FileBrowserHelpers_MoveDirectory_m97F8E459D43028605A977F6C63E2DD1AA0450F66,
	FileBrowserHelpers_RenameFile_m28EC14810CA6B6A17C271ABC2AC2BFA21DDE67D9,
	FileBrowserHelpers_RenameDirectory_m006A6B9AAECF8288AF9D689AC23C54F6E6518DEB,
	FileBrowserHelpers_DeleteFile_m9FE24F1EEA853DE05BE18E9DA85D014C73360AD8,
	FileBrowserHelpers_DeleteDirectory_m2812DF984D0E379CD2185D077FF519AB5FA717EB,
	FileBrowserHelpers_GetFilename_mA8E038218E294D76E4DAFD17571427175F8E24BE,
	FileBrowserHelpers_GetFilesize_mD732CD14CE50897F645BC05449BB668FFEB0D8A1,
	FileBrowserHelpers_GetLastModifiedDate_m36E0A235A22F2472CA30E88F848A44EB50C0B540,
	FileBrowserItem_get_Icon_mA236A217FDF67ABF9C1650452ADD466428416208,
	FileBrowserItem_get_TransformComponent_m54A7F99BE536DE843DFD8C2308122A7BA77CFFD9,
	FileBrowserItem_get_Name_mB4AB269ACE59C310314C422DE06883842C6B1A15,
	FileBrowserItem_get_IsDirectory_mE2B9AE6ADA43DF5A8408EE693F2FDDD88D216D0F,
	FileBrowserItem_set_IsDirectory_mA1AD3AFE7B0E46210FE1289FE2D291046DEF66F7,
	FileBrowserItem_SetFileBrowser_m113A09B2B5105907F7A0565AABD1208E96E1DD35,
	FileBrowserItem_SetFile_m01704B1D968B4F8DCFD399DC0D9FE8DC07A36DE5,
	FileBrowserItem_Update_m57C36405867A0ECD4291F78C6C0068A00C66C1E0,
	FileBrowserItem_OnPointerClick_mA105524535DA9304C54057B9FBEFC4DC607E4FEF,
	FileBrowserItem_OnPointerDown_mE37EAEE7FDF98BB1F7A9B349D3B3072A6132EC63,
	FileBrowserItem_OnPointerUp_mD4DAF0E75338718E388E09FB38D79C3A6B3A9616,
	FileBrowserItem_OnPointerEnter_m6E522B31AA73A624A7E2C1E0C072A2B9B37B5810,
	FileBrowserItem_OnPointerExit_m53AA3FB9005B0A92FA62C66BA1EED14DE61E1982,
	FileBrowserItem_SetSelected_mD98A715E8753CAAC93243CE7E865BB3E981FE3BB,
	FileBrowserItem_SetHidden_mC9BA839FA2A4429730135DC29989E8B441E76525,
	FileBrowserItem_OnSkinRefreshed_m9ACDA69EDA9861AC47C8A27F02984C03291C1051,
	FileBrowserItem__ctor_mFD76D8703595FF18AE3C0C30455FD2E877BC5DB8,
	FileBrowserMovement_Initialize_mE6E2DCBA906CED5E93919B74BACC873B18DC8C5A,
	FileBrowserMovement_OnDragStarted_mB878CDC08ABE141804FA0E6DC015D25C2F4E8C02,
	FileBrowserMovement_OnDrag_m3CA70167EDA0F00781090D5698F1B5B6A9D95F1B,
	FileBrowserMovement_OnEndDrag_m7CD70CC03FEF03871A31AECFE52D69AD85D4DB9B,
	FileBrowserMovement_OnResizeStarted_m7C591C6E5CDF08E378B0404CC6E491B93D4F6DEE,
	FileBrowserMovement_OnResize_m2F86FCF2FFFE5B82FD8F8E33400488B3AFC63165,
	FileBrowserMovement_OnEndResize_m8425FEDD990DD74A6574095F42D184CD6A5DD4CE,
	FileBrowserMovement__ctor_mD8568EF6C0AA08152B980E6D05BD4879030B4F8E,
	FileBrowserQuickLink_get_TargetPath_mFF4A822E3C9FFDE3F531E1376E6B1797D64AD756,
	FileBrowserQuickLink_SetQuickLink_mCA309400C8524097232B24883F14BAFE92D0C900,
	FileBrowserQuickLink__ctor_m5BDF5B0833F6CC163E18BCCFDF01CCE9DCADC3D1,
	FileBrowserRenamedItem_get_InputField_m91346D55198B298E74E3BCF1E0F0217F89BDDDD4,
	FileBrowserRenamedItem_get_TransformComponent_m5028BCA8A4B50F5F12E62676D6606F29DD8D54CD,
	FileBrowserRenamedItem_Show_m55D9787654356551E45B68F837AF154EB31613F0,
	FileBrowserRenamedItem_OnInputFieldEndEdit_m5A25798DA5AB1238A465A6F2FB29C87527402452,
	FileBrowserRenamedItem__ctor_m8638B851D48F656DD2878E36B40CED94587E9A92,
	OnRenameCompleted__ctor_m48E13B91ED45B15352E140198FC3415D9A888858,
	OnRenameCompleted_Invoke_m383F016C41D7C2B8E78DBA3BA5130DE979344E21,
	OnRenameCompleted_BeginInvoke_mEA6D5D7CE1A2D85340E70E28964064BFDD3247E2,
	OnRenameCompleted_EndInvoke_m296469B8DE1FD1150FA9EDC0F28A386571EA0F95,
	NonDrawingGraphic_SetMaterialDirty_m64684F184E0307DE955559C0E57F748B41B879A4,
	NonDrawingGraphic_SetVerticesDirty_m8B4982B91710D4046B2751FFD62A6B193A100AB6,
	NonDrawingGraphic_OnPopulateMesh_mE2F59B5470ACE70E2934C5A873CFE863F06E454C,
	NonDrawingGraphic__ctor_m561CB90871C92DBC8E0839BA9C92D5929551709F,
	OnItemClickedHandler__ctor_mF80210604B9059FD235A2C0339A14F815DF69314,
	OnItemClickedHandler_Invoke_m35BB1924A7FD2EDADA9D5BF729B37C3E1D7694EA,
	OnItemClickedHandler_BeginInvoke_mEDA83A6B63004CDECEEF29096BBF7F73BC00843B,
	OnItemClickedHandler_EndInvoke_mB9E3F49BD94DCB436D005D25DC07B28A5DD44AF1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ListItem_get_Tag_m54D91B8F78C5BF1987B21B392983057ECED6D383,
	ListItem_set_Tag_mF48352498EE087564986E5BF9E9CDACA78C342EA,
	ListItem_get_Position_m49214519D4BBE5B0F33D6AAB4E3B8F49B580DEFD,
	ListItem_set_Position_m8605AB6B00F33563B51063DAEE72D65110B2CBAD,
	ListItem_SetAdapter_m72EBAFC39C01195C647B1EC6E0A57D7D0B378D1E,
	ListItem_OnClick_m251412E71A6074C6C7BA2101009FC298987A481A,
	ListItem__ctor_m0F2F59D6C18F974CC520A3E73153EA9C25AE2353,
	RecycledListView_Start_m624BF5D74E7F55A5344C9ED80773F6CC601939DC,
	RecycledListView_SetAdapter_mF900098FEB7DCE2041BE234FD593A9DDD7E20699,
	RecycledListView_OnSkinRefreshed_mA211E24C82988639442BFFB7077CCC5073C7B3EC,
	RecycledListView_UpdateList_mDB5B2A69EA3A9DD770D372547E3F183554B3EE4F,
	RecycledListView_OnViewportDimensionsChanged_mB6C2DC3A7A045804CA21594CADB070B00EA8C04B,
	RecycledListView_UpdateItemsInTheList_m55C6ABE3F27C06D0AC7483627ED8D51FBAF83AF4,
	RecycledListView_CreateItemsBetweenIndices_m429BB2B660C88407528C6F8D3EDE86B7AC688E85,
	RecycledListView_CreateItemAtIndex_m0A47C8737153B21BFD8616E3DC9FC3E0FA678404,
	RecycledListView_DestroyItemsBetweenIndices_m595074E0ACC185C718E71BD88AFF7EB5C7DE6201,
	RecycledListView_UpdateItemContentsBetweenIndices_mE71583AFA6393C6B6A2358F76927FB069438B7C9,
	RecycledListView__ctor_m883D98DDCC2B6870B849DA84AB7245DE5CC3314F,
	RecycledListView_U3CStartU3Eb__10_0_mB8CB8298DCD1DFA739C0DD5C6394175D0C9D80EA,
	UISkin_get_Version_mDAF7A3DD2F270AF0E34DB56BE95481EFEBE3D150,
	UISkin_Invalidate_mBDEEA141046DC463BFFCE1E4E3F3EEEFF0DE066F,
	UISkin_get_Font_mF4B20BA9562F4CDEFE7F72CBA8A2EDC481DE800B,
	UISkin_set_Font_mDC0C97F15A599D1540DB2767935F94BC7689B1A7,
	UISkin_get_FontSize_mC5BD7D44ABDB28F2D804938F5CD1E6FE82970700,
	UISkin_set_FontSize_mF2097F11D9B90DA7031380B2AADD228B48017C3F,
	UISkin_get_WindowColor_m587EB65FACDE434DBCC5E2961D517FB0071F2B9B,
	UISkin_set_WindowColor_m87B53B161651DD26C478F3F33DF5F4625DCC8AE3,
	UISkin_get_FilesListColor_m7DAED2AD9BE8CDBA0C38BF2A153E8117B215A8DC,
	UISkin_set_FilesListColor_mCA788F63367DE069F8FD1664C7BEC1CD5872C00E,
	UISkin_get_FilesVerticalSeparatorColor_mC7398F4BC0B44798A0049B102FE5CF2ECA119A8B,
	UISkin_set_FilesVerticalSeparatorColor_m433BE8490F69502A0BC7957B6304A7E6AC2B903F,
	UISkin_get_TitleBackgroundColor_mE9842CD030A5C2711F49068385F3CF209162B175,
	UISkin_set_TitleBackgroundColor_m7D6171508315316F2854F03A4092CB68E7AF6069,
	UISkin_get_TitleTextColor_m0EFEAD3F0F07BC93BB02F3ED98399FF5DA701864,
	UISkin_set_TitleTextColor_m95C293827628CE2ABE320F18FBF3AB894B870F11,
	UISkin_get_WindowResizeGizmoColor_m05BE6C8ED3632A4A18C82E91C066A7B016AEC568,
	UISkin_set_WindowResizeGizmoColor_mBBBD0B6572664BD4970DB330E2EB39D5D1569BA2,
	UISkin_get_HeaderButtonsColor_m11E813AEA79CF48086C20C8049DA7CB8144A970C,
	UISkin_set_HeaderButtonsColor_mE7B34CFF77ACE5708979708D5601A43FB4FEE1B9,
	UISkin_get_WindowResizeGizmo_mDBEB8CE46B80527E0F33AE2173A305BFF3F9E18D,
	UISkin_set_WindowResizeGizmo_m1A4EC118F3F62BF5C11682E0652004AD2F2823B9,
	UISkin_get_HeaderBackButton_mBE8EA5E2A83099BE0D0200FA395E08F7DEB22EF7,
	UISkin_set_HeaderBackButton_m2105844942D1764173DE589E5B57DE198948A29D,
	UISkin_get_HeaderForwardButton_m8B3822B153FBE2F369C08E0C3CEFDC87EAB90826,
	UISkin_set_HeaderForwardButton_m1A9B67C4EF1E2EF6EE3868086978314F8D475AAD,
	UISkin_get_HeaderUpButton_m8462D0BB1A8D5AD173E463D6C200BBE5799268ED,
	UISkin_set_HeaderUpButton_m6F2590A5A6177E98192F4F8963802C1BCBFBBA3A,
	UISkin_get_HeaderContextMenuButton_m2C886896865E1BD03E061FE941C52410A5A3ADC8,
	UISkin_set_HeaderContextMenuButton_m1BCC96265FBBBA16D969AA3F42110247CB6FEEAB,
	UISkin_get_InputFieldNormalBackgroundColor_m3F438650067F2332CD8726F3EC1F902D374AFFDD,
	UISkin_set_InputFieldNormalBackgroundColor_mD9DE1BA2802C58B85F3A66EA9858BA0ABBE0F30F,
	UISkin_get_InputFieldInvalidBackgroundColor_mDB2E76A76311F129618DDFBA57C416EBE0C1CCF1,
	UISkin_set_InputFieldInvalidBackgroundColor_m5C4AA5F815E20730A067A2BE6ECE0B1000E4C0F2,
	UISkin_get_InputFieldTextColor_m2C432E5F4DD75770C2E537C8996BBC5A67691883,
	UISkin_set_InputFieldTextColor_mED924C2919107F7F6735B3C860DE20ECEBF5820E,
	UISkin_get_InputFieldPlaceholderTextColor_mA9480AE69A17149110E4B23FB68ECA33485C6207,
	UISkin_set_InputFieldPlaceholderTextColor_mA4F622FF2AB41D72C630DEBE09EBEF3969E747BA,
	UISkin_get_InputFieldSelectedTextColor_mD5FB460350A6BAB9D2B14DCF1679339134A97EBD,
	UISkin_set_InputFieldSelectedTextColor_m9E6F7796FD5ECD0A18DC3B62F6C6123D696D682E,
	UISkin_get_InputFieldCaretColor_mF94C7F5BAECD745F3955C0B4D348A16DEC4CE97C,
	UISkin_set_InputFieldCaretColor_mA5E1845D1927B4CDDF41C004664F706DF373E02A,
	UISkin_get_InputFieldBackground_m9E4B1FEB7DBCE7B3C6C3158B98D63335925407FF,
	UISkin_set_InputFieldBackground_m30454DC4852F1B8A716AF1836B6CF10411AFFAAA,
	UISkin_get_ButtonColor_m4CED62E6272B26B0D6C98534DB4313E467D323C9,
	UISkin_set_ButtonColor_mAAC143C605A6FC5FF81FC2513E975B7145A491E4,
	UISkin_get_ButtonTextColor_mA762705039E71DD73F2C9BBCE7DCF7E833BA0E3B,
	UISkin_set_ButtonTextColor_m2A6B52496D891249BA32642DFDAC7E405E09A203,
	UISkin_get_ButtonBackground_mB1D404AEED0B9774F30D674BD202CF1428A6A892,
	UISkin_set_ButtonBackground_mF775247FC33D64F36A787FC4F9C36CD829EB2DA8,
	UISkin_get_DropdownColor_mDF1DC9234C2695E1FE3F39CA35756F2B5DA7E42E,
	UISkin_set_DropdownColor_m1DC7F7EDD31E2A96C646FA2915E0278857C9AA1E,
	UISkin_get_DropdownTextColor_mBD5EDBD3E7DA244242139EA73EFB51AFB94B6E8D,
	UISkin_set_DropdownTextColor_mC6878CCF4E7016610DC10890EDB72D171D8404A7,
	UISkin_get_DropdownArrowColor_m0BAA9A62C446917A1588EB59F236FF2F5E1B7680,
	UISkin_set_DropdownArrowColor_mAAC237A078DE523909FD5B563EEF0494F4B602E9,
	UISkin_get_DropdownCheckmarkColor_mC51672265D4B4812EF665A7A272B534A315AD76A,
	UISkin_set_DropdownCheckmarkColor_m5E95085890C7AC2C1F7F6CF0C800AD9F05A8AD02,
	UISkin_get_DropdownBackground_m92ADAE1BE5D404AF633C4E01F34FB3C8D3B5C585,
	UISkin_set_DropdownBackground_mD5D5F5D3C3EB668C1C417031155759174C42EC5D,
	UISkin_get_DropdownArrow_m5B3CCA38E12871ADBF5C0490D8FFC086B38D471F,
	UISkin_set_DropdownArrow_m45A87A039D8B99588A59BC8BFA08B83B116CC970,
	UISkin_get_DropdownCheckmark_mDE2FA7C79BA64DFBE642D1FDE9D2ABBEAF92B194,
	UISkin_set_DropdownCheckmark_mF016CF62667E81CD19A3BF10880C6C3121C57E47,
	UISkin_get_ToggleColor_m9E7CC20BC528EA672166874622A88AEC017D7F0D,
	UISkin_set_ToggleColor_m9DB0C5918C8FF182490ADB1FF98D0030F308B58B,
	UISkin_get_ToggleTextColor_m7A38BE81665D996189C75FD7FA164B5C9C24DEDA,
	UISkin_set_ToggleTextColor_m02734327FE5CE4776CD73FCBB81D06C6607E5BDF,
	UISkin_get_ToggleCheckmarkColor_m3E3911F246186C227EF8058089B52C4F97511EDA,
	UISkin_set_ToggleCheckmarkColor_mF04576D2C1F3D3F880DEC8DB55FD3BAD5B287DFF,
	UISkin_get_ToggleBackground_m68C9BDE25EA7EF5B704E88E049BC2568B446C34F,
	UISkin_set_ToggleBackground_mFCAD87E2279264C9437719499842829923C87DF4,
	UISkin_get_ToggleCheckmark_m45AD35D157929952F77BE0B187DCDD93AEFEF8E1,
	UISkin_set_ToggleCheckmark_mDC026395E75DDA862CCF91D45E8C1502E7BF07FC,
	UISkin_get_ScrollbarBackgroundColor_m2476D94D639B09446FAB8AAC1680E82EC622ED78,
	UISkin_set_ScrollbarBackgroundColor_m2CB9CBC9FC10970DDABF5EA840CAB5096481689A,
	UISkin_get_ScrollbarColor_m06B0E4B4BEC11A0D56E10598DB5506B49D14133E,
	UISkin_set_ScrollbarColor_mC576252377DF2D2CBE5EB23F019422E7B5DD898D,
	UISkin_get_FileHeight_m490761AA2FCD12C90DE5F023FC3353965867FDF8,
	UISkin_set_FileHeight_m423EA00C6764B88317B6AA7FC57255C7FDD15E92,
	UISkin_get_FileIconsPadding_m3F6E9AB23B917C2804ECE55F73FF7ADD7591F0D5,
	UISkin_set_FileIconsPadding_m1A2819BC27847BEC67008DE8CD3DAF21C95A9C80,
	UISkin_get_FileNormalBackgroundColor_mB3AB273132682E837F5BFFDFDB3AF207D4C84A8A,
	UISkin_set_FileNormalBackgroundColor_m11EED12CB23F0A0F25A99F02EE0AAA06D50A033F,
	UISkin_get_FileAlternatingBackgroundColor_m9C9A101FE901B7A8E530ED22A1AC3F603184B7EF,
	UISkin_set_FileAlternatingBackgroundColor_m74BB407D9D0441C4256FB8BF96B8F8231C6F8FF5,
	UISkin_get_FileHoveredBackgroundColor_mBBFA2C11C2DE553F634A40D8B2EFA414D8BED2C6,
	UISkin_set_FileHoveredBackgroundColor_m489C9E0B2E0EA776DF57F571949280E32689914B,
	UISkin_get_FileSelectedBackgroundColor_m3E275E244C9580296423FEB3C26DF2D30C44BF37,
	UISkin_set_FileSelectedBackgroundColor_m471F5A3700D8881BA34709EE9533639BEC5EB0CD,
	UISkin_get_FileNormalTextColor_m858F74B10CCAF4618CA6E5C7167BAB80217D0A99,
	UISkin_set_FileNormalTextColor_mAA436E5280042DF16ADEFBE949E0EB9599C28FE5,
	UISkin_get_FileSelectedTextColor_mC1E14053CCF0BD5E03EC173BB7188B4DD0795EB4,
	UISkin_set_FileSelectedTextColor_m0F0EDD9CC86EC19F48FCFB4EE5F705047EA045AA,
	UISkin_get_FolderIcon_m0A907951175B18526FF527BDC3BDF52D06B3E6A4,
	UISkin_set_FolderIcon_m05531A4BA6947BEDC85E60329490D2CF1FEAB87E,
	UISkin_get_DriveIcon_mBEEB8049F062992480B4CDE6D40762778981EB6A,
	UISkin_set_DriveIcon_m525AB2878135C5B877DBC8EF7F2CE689C42D71F0,
	UISkin_get_DefaultFileIcon_m93D7C9CF7C1D84FD464C0F92FA9CE0F3589A9A29,
	UISkin_set_DefaultFileIcon_m4FE8383F04CD0B27B3B3C6EE7849F4D5EC929D65,
	UISkin_get_FiletypeIcons_mE72BFF3BDC905792B9447680D7697683EDE77D0E,
	UISkin_set_FiletypeIcons_mB29C8314A4C4979F4CEC787DE8B8E5C560A8708F,
	UISkin_get_AllIconExtensionsHaveSingleSuffix_m5C42381A1E46E9E52FFB01793EFC8B5C5055AEE4,
	UISkin_get_FileMultiSelectionToggleOffIcon_m03AB5F1FC3C182BBB9D891A48A74236D419AF857,
	UISkin_set_FileMultiSelectionToggleOffIcon_m8CA945920706E530712B2333CE608BE59BB6A2FA,
	UISkin_get_FileMultiSelectionToggleOnIcon_mD78F39CDD08F1D545BAB8F5206634F0D0FBE395C,
	UISkin_set_FileMultiSelectionToggleOnIcon_mD081AC1D7929EAB46898CA3C74548B0E567D15B1,
	UISkin_get_ContextMenuBackgroundColor_mB22BB4E4E1F1174790B7C5155A6A56C19ECBB545,
	UISkin_set_ContextMenuBackgroundColor_mE4D66E88E0AD25792D579BCF36D1E0FCDA140F52,
	UISkin_get_ContextMenuTextColor_m7442C31C4B35D1CC6BF57F8BB91C4C7AABE6E76A,
	UISkin_set_ContextMenuTextColor_m2BB01FB8EF5D6AAAC9755960FFE9D53CC408283F,
	UISkin_get_ContextMenuSeparatorColor_mD624EE6AEA7EB9986A1A0EBA099A218D9FDEB8B9,
	UISkin_set_ContextMenuSeparatorColor_m422511782E3018925AD56EFCC8E54E94572B3232,
	UISkin_get_DeletePanelBackgroundColor_m2BDD89F76C36F336A4EAB7CD1009A04842B6F1BA,
	UISkin_set_DeletePanelBackgroundColor_mD0C05EAA0D6E4D50394E17ED16F004B9E58F04F8,
	UISkin_get_DeletePanelTextColor_m43772B841F7B988833F46172DDC5EF0C9C2674EE,
	UISkin_set_DeletePanelTextColor_m61E85E92C56B748A37F09414120B040791699751,
	UISkin_get_DeletePanelBackground_m737D4F769A76B919552F437BA9625ACBBDF392B4,
	UISkin_set_DeletePanelBackground_m3F1A71803331D1F0D2445B83D4AF3668BBDF1B05,
	UISkin_ApplyTo_m53C98C362E95A9E5E22E385E0A39FEAFA2DA5DEB,
	UISkin_ApplyTo_m9B342028BE704955C675CC93864D4347FCF282BA,
	UISkin_ApplyTo_m74872228D165B123D86A4E2692C2027E337A6602,
	UISkin_ApplyTo_mD21874F09929E40C271F99AB71EA5EF2B91FD354,
	UISkin_ApplyTo_m90BBC4AB07D33B0EDD572D2CD6A92F52C353F493,
	UISkin_ApplyTo_m82C9BDB7381611FFF8AE75C42E41824F0E4E8E72,
	UISkin_GetIconForFileEntry_mBE3548DA513A0F0D809AFD07DC7067086E53E5AC,
	UISkin_InitializeFiletypeIcons_mA6B13A4B8D906480ABED33D8C70959FB25F94143,
	UISkin__ctor_m47C84F2C0A2F9BC4C013283F192B24251FD4340C,
};
extern void FileSystemEntry_get_IsDirectory_m5B6EAD6DAC01DC97FA18BF8C82FB57CD009F9E53_AdjustorThunk (void);
extern void FileSystemEntry__ctor_mFB6D7113F6EC140A0863D55B690ACADC5A22EBD1_AdjustorThunk (void);
extern void FileSystemEntry__ctor_mF3DAAE9ECE05D268F76C8A7A8E9B23083B699C22_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[3] = 
{
	{ 0x060000C0, FileSystemEntry_get_IsDirectory_m5B6EAD6DAC01DC97FA18BF8C82FB57CD009F9E53_AdjustorThunk },
	{ 0x060000C1, FileSystemEntry__ctor_mFB6D7113F6EC140A0863D55B690ACADC5A22EBD1_AdjustorThunk },
	{ 0x060000C2, FileSystemEntry__ctor_mF3DAAE9ECE05D268F76C8A7A8E9B23083B699C22_AdjustorThunk },
};
static const int32_t s_InvokerIndices[418] = 
{
	8655,
	7884,
	8655,
	7884,
	8675,
	7892,
	8675,
	7892,
	8655,
	7884,
	8655,
	7884,
	7892,
	7892,
	8687,
	7898,
	8655,
	7884,
	8655,
	7884,
	8675,
	7892,
	8675,
	7892,
	8675,
	7892,
	8675,
	4108,
	4174,
	3428,
	4174,
	3428,
	4108,
	3354,
	4153,
	3408,
	4108,
	3354,
	4108,
	3354,
	4174,
	3428,
	4174,
	3428,
	4174,
	3428,
	4274,
	4274,
	4274,
	4274,
	3354,
	4174,
	3428,
	4153,
	4214,
	4174,
	3428,
	4274,
	4274,
	4274,
	4274,
	4274,
	4274,
	4274,
	3509,
	1862,
	4274,
	4274,
	3428,
	3354,
	3428,
	3428,
	4274,
	4274,
	1801,
	3428,
	821,
	3428,
	1812,
	4274,
	3354,
	4274,
	4274,
	4274,
	4174,
	4274,
	4274,
	4274,
	731,
	4274,
	4274,
	4274,
	3509,
	3004,
	6386,
	3009,
	4274,
	765,
	768,
	728,
	2856,
	3009,
	4435,
	4435,
	4413,
	7884,
	4533,
	4533,
	5496,
	8700,
	7892,
	7884,
	6606,
	6606,
	6606,
	6606,
	7884,
	8700,
	7454,
	8668,
	8668,
	4274,
	8700,
	3428,
	4274,
	3428,
	1812,
	1812,
	1114,
	4174,
	1810,
	3428,
	802,
	3428,
	1810,
	4274,
	1385,
	3428,
	1810,
	2333,
	782,
	2377,
	8700,
	4274,
	1246,
	3408,
	4274,
	4108,
	4174,
	4274,
	4174,
	4274,
	3428,
	3408,
	4274,
	4108,
	4174,
	4274,
	4174,
	3408,
	4274,
	4108,
	4174,
	4274,
	4174,
	172,
	4274,
	3428,
	4274,
	4274,
	4274,
	4274,
	4274,
	4274,
	3428,
	3428,
	3428,
	3428,
	4274,
	4274,
	4274,
	647,
	3509,
	3428,
	4274,
	4274,
	4274,
	1810,
	4274,
	1385,
	3428,
	4108,
	645,
	1812,
	7454,
	7454,
	7454,
	7647,
	6386,
	6393,
	6393,
	6667,
	6667,
	6667,
	6667,
	6667,
	7647,
	7647,
	6667,
	6667,
	6667,
	6667,
	6667,
	6393,
	6393,
	7892,
	7892,
	7647,
	7597,
	7495,
	4174,
	4174,
	4174,
	4108,
	3354,
	1812,
	969,
	4274,
	3428,
	3428,
	3428,
	3428,
	3428,
	3354,
	3354,
	1801,
	4274,
	3428,
	3428,
	3428,
	3428,
	3428,
	3428,
	3428,
	4274,
	4174,
	972,
	4274,
	4174,
	4174,
	599,
	3428,
	4274,
	1810,
	3428,
	802,
	3428,
	4274,
	4274,
	3428,
	4274,
	1810,
	3428,
	802,
	3428,
	0,
	0,
	0,
	0,
	0,
	0,
	4174,
	3428,
	4153,
	3408,
	3428,
	4274,
	4274,
	4274,
	3428,
	4274,
	4274,
	4274,
	3354,
	1649,
	3408,
	1649,
	1649,
	4274,
	3509,
	4153,
	4274,
	4174,
	3428,
	4153,
	3408,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4174,
	3428,
	4174,
	3428,
	4174,
	3428,
	4174,
	3428,
	4174,
	3428,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4174,
	3428,
	4110,
	3357,
	4110,
	3357,
	4174,
	3428,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4174,
	3428,
	4174,
	3428,
	4174,
	3428,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4174,
	3428,
	4174,
	3428,
	4110,
	3357,
	4110,
	3357,
	4214,
	3463,
	4214,
	3463,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4174,
	3428,
	4174,
	3428,
	4174,
	3428,
	4174,
	3428,
	4108,
	4174,
	3428,
	4174,
	3428,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4110,
	3357,
	4174,
	3428,
	1803,
	3428,
	3428,
	3428,
	3428,
	3428,
	1371,
	4274,
	4274,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_SimpleFileBrowser_Runtime_CodeGenModule;
const Il2CppCodeGenModule g_SimpleFileBrowser_Runtime_CodeGenModule = 
{
	"SimpleFileBrowser.Runtime.dll",
	418,
	s_methodPointers,
	3,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
