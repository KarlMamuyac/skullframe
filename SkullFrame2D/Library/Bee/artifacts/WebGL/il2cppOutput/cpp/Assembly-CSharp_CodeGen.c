﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CharacterExporter::Start()
extern void CharacterExporter_Start_m6AD6E9A00B611B96B02C0D65307E06100A864A1E (void);
// 0x00000002 System.Void CharacterExporter::Awake()
extern void CharacterExporter_Awake_m5D21406A4D2917BAF9C82548CD8DB18F42DD30E8 (void);
// 0x00000003 System.Void CharacterExporter::OnPostRender()
extern void CharacterExporter_OnPostRender_m7535C120BCD0126E132ADE9C51197EC9391E2839 (void);
// 0x00000004 System.Void CharacterExporter::TakeScreenshot()
extern void CharacterExporter_TakeScreenshot_mF714C88C0C861FF0D1468A2BC12A31B557223C16 (void);
// 0x00000005 System.Void CharacterExporter::TakeScreenshot_Static()
extern void CharacterExporter_TakeScreenshot_Static_m8D5BEBDE2E00F69521B0291D397ED73F9D13A6E6 (void);
// 0x00000006 System.Void CharacterExporter::.ctor()
extern void CharacterExporter__ctor_m2E81572C8A7378A6DC13E11BA5E10FB71669F92C (void);
// 0x00000007 System.Void CharacterExporter/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mACF7510BD8DD3BB197594B90B2B841F3DD86D892 (void);
// 0x00000008 System.Void CharacterExporter/<>c__DisplayClass5_0::<OnPostRender>b__0(System.String[])
extern void U3CU3Ec__DisplayClass5_0_U3COnPostRenderU3Eb__0_m03446CCE76534923CE21E2AADFD1E887D883409C (void);
// 0x00000009 System.Void FileUploadScript::OpenExplorerPNG()
extern void FileUploadScript_OpenExplorerPNG_m098D1690C44191C0629DB963922C97B83D44D099 (void);
// 0x0000000A System.Void FileUploadScript::UploadPNG(System.String,System.Byte[])
extern void FileUploadScript_UploadPNG_mD474D76747C7667AB50BBA74186C78C1773A6E8B (void);
// 0x0000000B System.Void FileUploadScript::.ctor()
extern void FileUploadScript__ctor_m53ADF1E6BED084B090FF0FF7CAAEE3FD999B0A38 (void);
// 0x0000000C System.Void FileUploadScript::<OpenExplorerPNG>b__1_0(System.String[])
extern void FileUploadScript_U3COpenExplorerPNGU3Eb__1_0_mE64463C16756CA88A75CE6116D6E0533514E98AB (void);
// 0x0000000D UnityEngine.U2D.Animation.SpriteLibraryAsset Mixer::get_LibraryAsset()
extern void Mixer_get_LibraryAsset_mB6C5F53CCCCA39F3AB287BF48246B2DCA8A51A6E (void);
// 0x0000000E System.Void Mixer::Start()
extern void Mixer_Start_m1931E34D3474C351C6E0A39DF7406F74A96A4750 (void);
// 0x0000000F System.Void Mixer::AddUISelector(MixerCategory)
extern void Mixer_AddUISelector_m00080FA98E8058C715BECFE63E6770BA1FC53C18 (void);
// 0x00000010 System.Void Mixer::.ctor()
extern void Mixer__ctor_mA2A1E7303A016BE7A201DBDFE751D7EA2856A9B9 (void);
// 0x00000011 System.Void MixerSelector::Init(MixerCategory,System.String[])
extern void MixerSelector_Init_m737A0A246E150F73B749E8273CE9F6460DE49057 (void);
// 0x00000012 System.Void MixerSelector::AddDropdownOption()
extern void MixerSelector_AddDropdownOption_m1242476A767A9EA50AB1F8F9386AA2B22A51BDF7 (void);
// 0x00000013 System.Void MixerSelector::AddPNGasOption()
extern void MixerSelector_AddPNGasOption_m3AD3C69A621A6A7B091C6CB02B8AF8FC4EF088B4 (void);
// 0x00000014 System.Void MixerSelector::.ctor()
extern void MixerSelector__ctor_mF514279B562F8CC6AA1281ADDB50A872D76C5DCE (void);
// 0x00000015 System.Void MixerSelector/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m5A5593081159AC60BC30FADA8242BE8123F8786C (void);
// 0x00000016 System.Void MixerSelector/<>c__DisplayClass2_0::<Init>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass2_0_U3CInitU3Eb__0_m879DFC5134E4D64B55A1C27A021BE1BF26776399 (void);
// 0x00000017 System.Void PartSwapUI::Start()
extern void PartSwapUI_Start_m1F3B6AF10CDF5EB581D9882E8CC66867F6880453 (void);
// 0x00000018 System.Void PartSwapUI::SetDefaultPanels()
extern void PartSwapUI_SetDefaultPanels_mB9AAC4B929365ED6543E88E9726BDF8D187378C9 (void);
// 0x00000019 System.Void PartSwapUI::SetUIComponent()
extern void PartSwapUI_SetUIComponent_mA49F292D96E393F22A07E632CAFC48AA726D28A4 (void);
// 0x0000001A System.Void PartSwapUI::SetColorParameters()
extern void PartSwapUI_SetColorParameters_m72BC9E9C784D3B8E00C34F3C36BF790BB9195097 (void);
// 0x0000001B UnityEngine.Color PartSwapUI::HexToColor(System.String)
extern void PartSwapUI_HexToColor_m1586E81E7BA91791E6B2E8658E9B7FF2AD6B2E52 (void);
// 0x0000001C System.Void PartSwapUI::SetBodyPanelActive()
extern void PartSwapUI_SetBodyPanelActive_m27E49102C7E29AFFD4DA16D001D605CB4DE72A5C (void);
// 0x0000001D System.Void PartSwapUI::SetFacePanelActive()
extern void PartSwapUI_SetFacePanelActive_m69DFB6094B79B2F0223D593307D2C98571287021 (void);
// 0x0000001E System.Void PartSwapUI::ChangeSwapperPanel(System.String)
extern void PartSwapUI_ChangeSwapperPanel_mD23593010F103447865A24423253DC6F4FBE818F (void);
// 0x0000001F System.Void PartSwapUI::.ctor()
extern void PartSwapUI__ctor_m7899F81721B871AABFFCE80E8796474C2C8175D7 (void);
// 0x00000020 System.Void PoseSelector::Start()
extern void PoseSelector_Start_m0B67E3B6EFBF281D08DB0965D1CE6E605A5A2D13 (void);
// 0x00000021 System.Void PoseSelector::CreatePoseOptions(UnityEngine.Animator)
extern void PoseSelector_CreatePoseOptions_mEE41941444B7EB5EC4751A52FC15D4E8E8799090 (void);
// 0x00000022 System.Void PoseSelector::.ctor()
extern void PoseSelector__ctor_mEB865B2131255A286AB0145D6B1D263110230D41 (void);
// 0x00000023 System.Void PoseSelector::<CreatePoseOptions>b__5_0(System.Int32)
extern void PoseSelector_U3CCreatePoseOptionsU3Eb__5_0_m718B43F1040988AE39A499812058DC50C6090EBC (void);
// 0x00000024 UnityEngine.U2D.Animation.SpriteLibraryAsset Swapper::get_LibraryAsset()
extern void Swapper_get_LibraryAsset_m75B6B3285CD2183607599C1CF74A38521C6E5198 (void);
// 0x00000025 System.Void Swapper::SelectRandom()
extern void Swapper_SelectRandom_m6DE2507D1372158BA2F97598B2917615D466AF6B (void);
// 0x00000026 System.Void Swapper::LoadUploadedSprite(System.String,System.String)
extern void Swapper_LoadUploadedSprite_mFD4DF74492D8853A498EDBADB838EC2BD080372C (void);
// 0x00000027 System.Void Swapper::.ctor()
extern void Swapper__ctor_mA247E45A6F8F61F1E16698BBD20B9C41492D374B (void);
// 0x00000028 System.Void UIHandlerScript::Start()
extern void UIHandlerScript_Start_m1C2D63405BF13B5ED9D8E9BBA8C03DDC5464F63B (void);
// 0x00000029 System.Void UIHandlerScript::SetDefaultPanels()
extern void UIHandlerScript_SetDefaultPanels_m4D32171F188ED2D3B979D4988FDD3DB6F1B8AD9C (void);
// 0x0000002A System.Void UIHandlerScript::SetUIComponent()
extern void UIHandlerScript_SetUIComponent_mA527F8AA06BD9FDDC6C811ECA48D77D0617F5884 (void);
// 0x0000002B System.Void UIHandlerScript::SetColorParameters()
extern void UIHandlerScript_SetColorParameters_m824165107C61A14C6E5FC0C94F0EB0F96C6D83C2 (void);
// 0x0000002C UnityEngine.Color UIHandlerScript::HexToColor(System.String)
extern void UIHandlerScript_HexToColor_m11122AFD02CE9BD49FBCC1C5441C7E41251A6062 (void);
// 0x0000002D System.Void UIHandlerScript::SetLuchaPanelActive()
extern void UIHandlerScript_SetLuchaPanelActive_m70B03A1ED721E7B68FA7FBA5093EF11C421DAD87 (void);
// 0x0000002E System.Void UIHandlerScript::SetSamplePanelActive()
extern void UIHandlerScript_SetSamplePanelActive_mCBA7829DBE759464B3A77820E9C6D985A81003B8 (void);
// 0x0000002F System.Void UIHandlerScript::ChangeSwapperPanel(System.String)
extern void UIHandlerScript_ChangeSwapperPanel_m1CF5D2EBAE153205D3DB67148F03553E1B9059F1 (void);
// 0x00000030 System.Void UIHandlerScript::.ctor()
extern void UIHandlerScript__ctor_m26492FF99AC95C5ED189769655E42C171307A5C1 (void);
static Il2CppMethodPointer s_methodPointers[48] = 
{
	CharacterExporter_Start_m6AD6E9A00B611B96B02C0D65307E06100A864A1E,
	CharacterExporter_Awake_m5D21406A4D2917BAF9C82548CD8DB18F42DD30E8,
	CharacterExporter_OnPostRender_m7535C120BCD0126E132ADE9C51197EC9391E2839,
	CharacterExporter_TakeScreenshot_mF714C88C0C861FF0D1468A2BC12A31B557223C16,
	CharacterExporter_TakeScreenshot_Static_m8D5BEBDE2E00F69521B0291D397ED73F9D13A6E6,
	CharacterExporter__ctor_m2E81572C8A7378A6DC13E11BA5E10FB71669F92C,
	U3CU3Ec__DisplayClass5_0__ctor_mACF7510BD8DD3BB197594B90B2B841F3DD86D892,
	U3CU3Ec__DisplayClass5_0_U3COnPostRenderU3Eb__0_m03446CCE76534923CE21E2AADFD1E887D883409C,
	FileUploadScript_OpenExplorerPNG_m098D1690C44191C0629DB963922C97B83D44D099,
	FileUploadScript_UploadPNG_mD474D76747C7667AB50BBA74186C78C1773A6E8B,
	FileUploadScript__ctor_m53ADF1E6BED084B090FF0FF7CAAEE3FD999B0A38,
	FileUploadScript_U3COpenExplorerPNGU3Eb__1_0_mE64463C16756CA88A75CE6116D6E0533514E98AB,
	Mixer_get_LibraryAsset_mB6C5F53CCCCA39F3AB287BF48246B2DCA8A51A6E,
	Mixer_Start_m1931E34D3474C351C6E0A39DF7406F74A96A4750,
	Mixer_AddUISelector_m00080FA98E8058C715BECFE63E6770BA1FC53C18,
	Mixer__ctor_mA2A1E7303A016BE7A201DBDFE751D7EA2856A9B9,
	MixerSelector_Init_m737A0A246E150F73B749E8273CE9F6460DE49057,
	MixerSelector_AddDropdownOption_m1242476A767A9EA50AB1F8F9386AA2B22A51BDF7,
	MixerSelector_AddPNGasOption_m3AD3C69A621A6A7B091C6CB02B8AF8FC4EF088B4,
	MixerSelector__ctor_mF514279B562F8CC6AA1281ADDB50A872D76C5DCE,
	U3CU3Ec__DisplayClass2_0__ctor_m5A5593081159AC60BC30FADA8242BE8123F8786C,
	U3CU3Ec__DisplayClass2_0_U3CInitU3Eb__0_m879DFC5134E4D64B55A1C27A021BE1BF26776399,
	PartSwapUI_Start_m1F3B6AF10CDF5EB581D9882E8CC66867F6880453,
	PartSwapUI_SetDefaultPanels_mB9AAC4B929365ED6543E88E9726BDF8D187378C9,
	PartSwapUI_SetUIComponent_mA49F292D96E393F22A07E632CAFC48AA726D28A4,
	PartSwapUI_SetColorParameters_m72BC9E9C784D3B8E00C34F3C36BF790BB9195097,
	PartSwapUI_HexToColor_m1586E81E7BA91791E6B2E8658E9B7FF2AD6B2E52,
	PartSwapUI_SetBodyPanelActive_m27E49102C7E29AFFD4DA16D001D605CB4DE72A5C,
	PartSwapUI_SetFacePanelActive_m69DFB6094B79B2F0223D593307D2C98571287021,
	PartSwapUI_ChangeSwapperPanel_mD23593010F103447865A24423253DC6F4FBE818F,
	PartSwapUI__ctor_m7899F81721B871AABFFCE80E8796474C2C8175D7,
	PoseSelector_Start_m0B67E3B6EFBF281D08DB0965D1CE6E605A5A2D13,
	PoseSelector_CreatePoseOptions_mEE41941444B7EB5EC4751A52FC15D4E8E8799090,
	PoseSelector__ctor_mEB865B2131255A286AB0145D6B1D263110230D41,
	PoseSelector_U3CCreatePoseOptionsU3Eb__5_0_m718B43F1040988AE39A499812058DC50C6090EBC,
	Swapper_get_LibraryAsset_m75B6B3285CD2183607599C1CF74A38521C6E5198,
	Swapper_SelectRandom_m6DE2507D1372158BA2F97598B2917615D466AF6B,
	Swapper_LoadUploadedSprite_mFD4DF74492D8853A498EDBADB838EC2BD080372C,
	Swapper__ctor_mA247E45A6F8F61F1E16698BBD20B9C41492D374B,
	UIHandlerScript_Start_m1C2D63405BF13B5ED9D8E9BBA8C03DDC5464F63B,
	UIHandlerScript_SetDefaultPanels_m4D32171F188ED2D3B979D4988FDD3DB6F1B8AD9C,
	UIHandlerScript_SetUIComponent_mA527F8AA06BD9FDDC6C811ECA48D77D0617F5884,
	UIHandlerScript_SetColorParameters_m824165107C61A14C6E5FC0C94F0EB0F96C6D83C2,
	UIHandlerScript_HexToColor_m11122AFD02CE9BD49FBCC1C5441C7E41251A6062,
	UIHandlerScript_SetLuchaPanelActive_m70B03A1ED721E7B68FA7FBA5093EF11C421DAD87,
	UIHandlerScript_SetSamplePanelActive_mCBA7829DBE759464B3A77820E9C6D985A81003B8,
	UIHandlerScript_ChangeSwapperPanel_m1CF5D2EBAE153205D3DB67148F03553E1B9059F1,
	UIHandlerScript__ctor_m26492FF99AC95C5ED189769655E42C171307A5C1,
};
static const int32_t s_InvokerIndices[48] = 
{
	4274,
	4274,
	4274,
	4274,
	8700,
	4274,
	4274,
	3428,
	4274,
	1812,
	4274,
	3428,
	4174,
	4274,
	3425,
	4274,
	1797,
	4274,
	4274,
	4274,
	4274,
	3408,
	4274,
	4274,
	4274,
	4274,
	2599,
	4274,
	4274,
	3428,
	4274,
	4274,
	3428,
	4274,
	3408,
	4174,
	4274,
	1812,
	4274,
	4274,
	4274,
	4274,
	4274,
	2599,
	4274,
	4274,
	3428,
	4274,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	48,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
